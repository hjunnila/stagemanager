include(../variables.pri)

TEMPLATE = subdirs
TARGET = documentation

documentation.files = index.html tutorial.html COPYING.html
documentation.path = $$DOCSDIR

INSTALLS += documentation
