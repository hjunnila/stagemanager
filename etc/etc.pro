TEMPLATE	= subdirs
CONFIG		+= ordered
TARGET		= icons

include(../variables.pri)

# Generic unix
unix:!macx {
	desktop.path	 = $$DATADIR/applications
	desktop.files 	 = stagemanager.desktop
	INSTALLS	+= desktop

	icons.path	 = $$DATADIR/pixmaps
	icons.files	 = ../gfx/stagemanager.png
	INSTALLS	+= icons

	samples.path	 = $$DATADIR
	samples.files	 = Sample.smv
	INSTALLS	+= samples

	xlations.path 	 = $$DATADIR
	xlations.files 	 = ../src/*.qm
	INSTALLS	+= translations
}

# Mac OSX
macx {
	icns.path	= $$DATADIR
	#icns.commands 	+= sips -s format icns ../gfx/stagemanager.png --out stagemanager.icns &&
	icns.files	+= stagemanager.icns
	INSTALLS	+= icns

	#icnscp.path	= $$DATADIR
	#icnscp.files	+= stagemanager.icns
	#INSTALLS	+= icnscp

	plist.path	= $$INSTALLROOT
	plist.files	+= Info.plist
	INSTALLS	+= plist

	samples.path	 = $$DATADIR
	samples.files	 = Sample.smv
	INSTALLS	+= samples

	xlations.path 	 = $$DATADIR/translations
	xlations.files 	 = ../src/*.qm
	INSTALLS	+= xlations
}

# Win32 additional DLL installation
win32 {
	# Qt Libraries
	qtlibs.path  = $$BINDIR
	release:qtlibs.files = $$(QTDIR)/bin/QtCore4.dll \
			$$(QTDIR)/bin/QtGui4.dll \
			$$(QTDIR)/bin/QtXml4.dll
	debug:qtlibs.files = $$(QTDIR)/bin/QtCored4.dll \
			$$(QTDIR)/bin/QtGuid4.dll \
			$$(QTDIR)/bin/QtXmld4.dll
	INSTALLS += qtlibs

	# MinGW library
	mingw.path      = $$BINDIR
	exists($$(SystemDrive)/MinGW/bin/mingwm10.dll) {
		mingw.files += $$(SystemDrive)/MinGW/bin/mingwm10.dll
	}
	exists($$(QTDIR)/../MinGW/bin/mingwm10.dll) {
		mingw.files += $$(QTDIR)/../MinGW/bin/mingwm10.dll
	}
	INSTALLS += mingw

	# GCC 4.4.0
	exists($$(SystemDrive)/MinGW/bin/libgcc_s_dw2-1.dll) {
		libgcc.files += $$(SystemDrive)/MinGW/bin/libgcc_s_dw2-1.dll
	}
	exists($$(QTDIR)/../MinGW/bin/libgcc_s_dw2-1.dll) {
		libgcc.files += $$(QTDIR)/../MinGW/bin/libgcc_s_dw2-1.dll
	}
	INSTALLS += libgcc

	# NullSoft installer files
	nsis.path  = $$DATADIR
	nsis.files = stagemanager.nsi
	INSTALLS  += nsis

	# Samples
	samples.path  = $$DATADIR
	samples.files = Sample.smv
	INSTALLS     += samples
}
