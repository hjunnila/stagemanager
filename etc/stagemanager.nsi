Name "Stage Manager"
OutFile "stagemanager-1.1.exe"
InstallDir C:\StageManager
RequestExecutionLevel user
;--------------------------------
; Pages
Page directory

Page custom StartMenuGroupSelect "" ": Start Menu Folder"
Function StartMenuGroupSelect
	Push $R1

	StartMenu::Select /checknoshortcuts "Don't create a start menu folder" /autoadd /lastused $R0 "Stage Manager"
	Pop $R1

	StrCmp $R1 "success" success
	StrCmp $R1 "cancel" done
		; error
		MessageBox MB_OK $R1
		StrCpy $R0 "Stage Manager" # use default
		Return
	success:
	Pop $R0

	done:
	Pop $R1
FunctionEnd

Page instfiles
Section
	SetOutPath $INSTDIR

	# this part is only necessary if you used /checknoshortcuts
	StrCpy $R1 $R0 1
	StrCmp $R1 ">" skip
		CreateDirectory $SMPROGRAMS\$R0
		CreateShortCut '$SMPROGRAMS\$R0\Stage Manager.lnk' $INSTDIR\stagemanager.exe

	skip:
SectionEnd

;--------------------------------
Section ""
  File stagemanager.exe
  File mingwm10.dll
  File libgcc_s_dw2-1.dll
  File QtCore4.dll
  File QtGui4.dll
  File QtXml4.dll
  File /r documentation
  File Sample.smv
  File stagemanager_fi_FI.qm
SectionEnd
