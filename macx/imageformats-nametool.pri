# Image format plugins depend on various Qt libraries
IMAGEFORMATS_DIR = /Developer/Applications/Qt/plugins/imageformats
imageformats.path = $$INSTALLROOT/Plugins

for(i, QTPLUGIN):{
        FILE = lib$${i}.dylib
	imageformats.files += $$IMAGEFORMATS_DIR/$$FILE
        qtnametool.commands += && $$LIBQTCORE_INSTALL_NAME_TOOL $$INSTALLROOT/Plugins/$$FILE
        qtnametool.commands += && $$LIBQTGUI_INSTALL_NAME_TOOL $$INSTALLROOT/Plugins/$$FILE
        qtnametool.commands += && $$LIBQTXML_INSTALL_NAME_TOOL $$INSTALLROOT/Plugins/$$FILE
	qtnametool.commands += && install_name_tool -id @executable_path/../Plugins/$$FILE $$INSTALLROOT/Plugins/$$FILE

	isEmpty(nametool.commands) {
	} else {
		nametool.commands += "&&"
	}

	nametool.commands += install_name_tool -change $$FILE \
				@executable_path/../Plugins/$$FILE $$OUTFILE
}
