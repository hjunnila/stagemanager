/*
  StageManager
  app.cpp

  Copyright (C) Heikki Junnila

  This program is free software; you can redistribute it and/or
  modify it under the terms of the GNU General Public License
  Version 2 as published by the Free Software Foundation.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details. The license is
  in the file "COPYING".

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*/

#include <QDesktopWidget>
#include <QMdiSubWindow>
#include <QApplication>
#include <QInputDialog>
#include <QCloseEvent>
#include <QMessageBox>
#include <QToolButton>
#include <QFileDialog>
#include <QDockWidget>
#include <QSettings>
#include <QToolTip>
#include <QMenuBar>
#include <QToolBar>
#include <QMdiArea>
#include <QTimer>
#include <QTime>
#include <QIcon>
#include <QList>
#include <QMenu>

#include "app.h"
#include "venue.h"
#include "stagevieweditor.h"
#include "venuemonitor.h"
#include "venueeditor.h"
#include "helpbrowser.h"
#include "stageview.h"
#include "aboutbox.h"
#include "timeview.h"

App::App(QWidget* parent) : QMainWindow(parent)
{
    m_timer = NULL;
    m_venue = NULL;

    m_venueEditor = NULL;
    m_venueMonitor = NULL;
    m_venueMonitorDock = NULL;
    m_stageView = NULL;
    m_stageMonitor = NULL;
    m_timeView = NULL;

    setWindowIcon(QIcon(":/gfx/stagemanager.png"));
    setCentralWidget(new QMdiArea(this));

    initActions();
    initMenuBar();
    initToolBar();

    setVenue(NULL);

    startMasterTimer();
    loadDefaults();
}

App::~App()
{
    saveDefaults();
    stopMasterTimer();
}

void App::loadDefaults()
{
    QSettings settings(KApplicationName, KApplicationName);
    QPoint pos = settings.value(KApplicationName + "/mainwindow/position",
                    QPoint(0, 0)).toPoint();
    QSize size = settings.value(KApplicationName + "/mainwindow/size",
                    QSize(800, 600)).toSize();
    resize(size);
    move(pos);
}

void App::saveDefaults()
{
    QSettings settings(KApplicationName, KApplicationName);
    settings.setValue(KApplicationName + "/mainwindow/position", pos());
    settings.setValue(KApplicationName + "/mainwindow/size", size());
}

void App::closeEvent(QCloseEvent* e)
{
    QString str;

    str = tr("Are you sure you wish to close %1?").arg(KApplicationName);
    if (m_venue != NULL && m_venue->isModified() == true)
        str += "\n" + tr("There are unsaved changes.");

    int r = QMessageBox::question(this,
                      tr("Close %1?").arg(KApplicationName),
                      str, QMessageBox::Yes, QMessageBox::No);
    if (r == QMessageBox::No)
    {
        e->ignore();
    }
    else
    {
        slotStageHide();
        e->accept();
    }
}

/*****************************************************************************
 * Venue
 *****************************************************************************/

Venue* App::venue() const
{
    return m_venue;
}

void App::newVenue()
{
    QString name;
    bool ok = false;

    /* Get a new name for the new venue */
    name = QInputDialog::getText(centralWidget(),
                     tr("New Venue"),
                     tr("Venue Name"), QLineEdit::Normal,
                     tr("New Venue"), &ok);

    /* Create a new venue and get rid of the old one only if the user
       presses ok in the name dialog */
    if (ok == true)
        setVenue(new Venue(this, name));
}

void App::setVenue(Venue* venue)
{
    Q_ASSERT(venue != NULL);

    if (m_stageMonitor != NULL)
        m_stageMonitor->setVenue(venue);
    if (m_stageView != NULL)
        m_stageView->setVenue(venue);
    if (m_venueMonitor != NULL)
        m_venueMonitor->setVenue(venue);
    if (m_venueEditor != NULL)
        m_venueEditor->setVenue(venue);

    if (m_venue != NULL)
        delete m_venue;
    m_venue = venue;

    if (venue != NULL)
    {
        m_fileSaveAction->setEnabled(true);
        m_fileSaveAsAction->setEnabled(true);

        m_venueEditorAction->setEnabled(true);
        m_venueMonitorAction->setEnabled(true);
        m_venueRenameAction->setEnabled(true);

        m_stageShowAction->setEnabled(true);
        m_stageHideAction->setEnabled(false);
        m_stageMonitorAction->setEnabled(true);
        m_stageViewEditorAction->setEnabled(true);

        slotVenueStateChanged(Venue::Stopped);

        connect(m_venue, SIGNAL(modified(bool)),
            this, SLOT(slotVenueModified(bool)));

        connect(m_venue, SIGNAL(stateChanged(Venue::State)),
            this, SLOT(slotVenueStateChanged(Venue::State)));

        connect(m_timer, SIGNAL(timeout()),
            m_venue, SLOT(slotMasterTick()));

        venue->setModified(false);
    }
    else
    {
        m_fileSaveAction->setEnabled(false);
        m_fileSaveAsAction->setEnabled(false);

        m_venueEditorAction->setEnabled(false);
        m_venueMonitorAction->setEnabled(false);

        m_stageShowAction->setEnabled(false);
        m_stageHideAction->setEnabled(false);
        m_stageMonitorAction->setEnabled(false);
        m_stageViewEditorAction->setEnabled(false);

        m_venueRenameAction->setEnabled(false);

        m_venueRunAction->setEnabled(false);
        m_venuePauseAction->setEnabled(false);
        m_venueStopAction->setEnabled(false);

        m_venuePreviousAction->setEnabled(false);
        m_venueNextAction->setEnabled(false);
    }

    updateWindowTitle();
}

void App::updateWindowTitle()
{
    if (m_venue != NULL)
    {
        if (m_venue->isModified() == true)
        {
            setWindowTitle(KApplicationName + " - " +
                       m_venue->name() + QString("*"));
        }
        else
        {
            setWindowTitle(KApplicationName + " - " +
                       m_venue->name());
        }
    }
    else
    {
        setWindowTitle(KApplicationName + " - " +
                   tr("Click new or open to begin"));
    }
}

void App::slotVenueModified(bool /*modified*/)
{
    updateWindowTitle();
}

void App::slotVenueStateChanged(Venue::State state)
{
    if (state == Venue::Running)
    {
        m_venueRunAction->setEnabled(false);
        m_venuePauseAction->setEnabled(true);
        m_venueStopAction->setEnabled(true);
        m_venuePreviousAction->setEnabled(true);
        m_venueNextAction->setEnabled(true);
    }
    else if (state == Venue::Paused)
    {
        m_venueRunAction->setEnabled(true);
        m_venuePauseAction->setEnabled(false);
        m_venueStopAction->setEnabled(true);
        m_venuePreviousAction->setEnabled(false);
        m_venueNextAction->setEnabled(false);
    }
    else
    {
        m_venueRunAction->setEnabled(true);
        m_venuePauseAction->setEnabled(false);
        m_venueStopAction->setEnabled(false);
        m_venuePreviousAction->setEnabled(false);
        m_venueNextAction->setEnabled(false);
    }
}

/*****************************************************************************
 * Master timer
 *****************************************************************************/

void App::startMasterTimer()
{
    Q_ASSERT(m_timer == NULL);
    m_timer = new QTimer(this);
    connect(m_timer, SIGNAL(timeout()), this, SLOT(slotMasterTimeout()));
    m_timer->start(1000);
}

void App::stopMasterTimer()
{
    Q_ASSERT(m_timer != NULL);
    m_timer->stop();
    delete m_timer;
    m_timer = NULL;
}

void App::slotMasterTimeout()
{
    emit masterTick();
}

/*****************************************************************************
 * File actions
 *****************************************************************************/

void App::initFileActions()
{
    /* File actions */
    m_fileNewAction = new QAction(QIcon(":/gfx/filenew.png"), tr("New"),
                      this);
    connect(m_fileNewAction, SIGNAL(triggered(bool)),
        this, SLOT(slotFileNew()));

    m_fileOpenAction = new QAction(QIcon(":/gfx/fileopen.png"), tr("Open"),
                       this);
    m_fileOpenAction->setShortcut(QKeySequence(Qt::CTRL + Qt::Key_O));
    connect(m_fileOpenAction, SIGNAL(triggered(bool)),
        this, SLOT(slotFileOpen()));

    m_fileSaveAction = new QAction(QIcon(":/gfx/filesave.png"), tr("Save"),
                       this);
    m_fileSaveAction->setShortcut(QKeySequence(Qt::CTRL + Qt::Key_S));
    connect(m_fileSaveAction, SIGNAL(triggered(bool)),
        this, SLOT(slotFileSave()));

    m_fileSaveAsAction = new QAction(QIcon(":/gfx/filesaveas.png"),
                     tr("Save As"), this);
    m_fileSaveAsAction->setShortcut(QKeySequence(Qt::CTRL + Qt::SHIFT +
                             Qt::Key_S));
    connect(m_fileSaveAsAction, SIGNAL(triggered(bool)),
        this, SLOT(slotFileSaveAs()));

    m_fileQuitAction = new QAction(QIcon(":/gfx/exit.png"), tr("Exit"),
                       this);
    m_fileQuitAction->setShortcut(QKeySequence(Qt::ALT + Qt::Key_F4));
    connect(m_fileQuitAction, SIGNAL(triggered(bool)),
        this, SLOT(slotFileQuit()));
}

void App::initFileMenu()
{
    m_fileMenu = new QMenu(menuBar());
    m_fileMenu->setTitle(tr("File"));
    m_fileMenu->addAction(m_fileNewAction);
    m_fileMenu->addAction(m_fileOpenAction);
    m_fileMenu->addSeparator();
    m_fileMenu->addAction(m_fileSaveAction);
    m_fileMenu->addAction(m_fileSaveAsAction);
    m_fileMenu->addSeparator();
    m_fileMenu->addAction(m_fileQuitAction);
}

void App::slotFileNew()
{
    /* Is the user sure? */
    if (m_venue != NULL)
    {
        int r = QMessageBox::question(
            this,
            tr("New Venue"),
            tr("Do you wish to discard the current venue (%1)?")
            .arg(m_venue->name()),
            QMessageBox::Yes, QMessageBox::No);

        if (r == QMessageBox::No)
            return;
    }

    newVenue();
}

void App::slotFileOpen()
{
    QString fileName;
    fileName = QFileDialog::getOpenFileName(this, tr("Open Venue"),
                        QString::null, 
                        QString("*") + KExtVenue);

    if (fileName.isEmpty() == false)
    {
        Venue* venue = Venue::loader(this, fileName);
        if (venue != NULL)
            setVenue(venue);
        else
            QMessageBox::warning(
                this,
                tr("Unable to load venue"),
                tr("File may be corrupt or you don't have permission to read it"));
    }
}

void App::slotFileSave()
{
    QString error;

    if (m_venue == NULL)
        return;

    if (m_venue->fileName().isEmpty() == true)
    {
        slotFileSaveAs();
    }
    else if (m_venue->save(error) == false)
    {
        QMessageBox::warning(centralWidget(), tr("Save failed"),
                     error);
        slotFileSaveAs();
    }
}

void App::slotFileSaveAs()
{
    QString fileName;
    QString error;

    if (m_venue == NULL)
        return;

    fileName = QFileDialog::getSaveFileName(this, tr("Save Venue As..."),
                        m_venue->fileName(), 
                        QString("*") + KExtVenue);
    if (fileName.isEmpty() == false)
    {
        if (m_venue->save(error, fileName) == false)
        {
            QMessageBox::warning(centralWidget(),
                         tr("Save failed"), error);
        }
    }
}

void App::slotFileQuit()
{
    close();
}

/*****************************************************************************
 * Venue actions
 *****************************************************************************/

void App::initVenueActions()
{
    m_venueEditorAction = new QAction(QIcon(":/gfx/venue.png"),
                      tr("Edit Venue"), this);
    connect(m_venueEditorAction, SIGNAL(triggered(bool)),
        this, SLOT(slotVenueEditor()));

    m_venueRenameAction = new QAction(QIcon(":/gfx/edit.png"),
                      tr("Rename Venue"), this);
    connect(m_venueRenameAction, SIGNAL(triggered(bool)),
        this, SLOT(slotVenueRename()));

    m_venueMonitorAction = new QAction(QIcon(":/gfx/venuemonitor.png"),
                       tr("Venue Monitor"), this);
    connect(m_venueMonitorAction, SIGNAL(triggered(bool)),
        this, SLOT(slotVenueMonitor()));

    m_venueTimeAction = new QAction(QIcon(":/gfx/timer.png"),
                    tr("Current Time"), this);
    connect(m_venueTimeAction, SIGNAL(triggered(bool)),
        this, SLOT(slotVenueTime()));

    m_venueRunAction = new QAction(QIcon(":/gfx/run.png"),
                       tr("Run"), this);
    connect(m_venueRunAction, SIGNAL(triggered(bool)),
        this, SLOT(slotVenueRun()));

    m_venuePauseAction = new QAction(QIcon(":/gfx/pause.png"),
                     tr("Pause"), this);
    connect(m_venuePauseAction, SIGNAL(triggered(bool)),
        this, SLOT(slotVenuePause()));

    m_venueStopAction = new QAction(QIcon(":/gfx/stop.png"),
                    tr("Stop"), this);
    connect(m_venueStopAction, SIGNAL(triggered(bool)),
        this, SLOT(slotVenueStop()));

    m_venuePreviousAction = new QAction(QIcon(":/gfx/previous.png"),
                        tr("Previous cue"), this);
    connect(m_venuePreviousAction, SIGNAL(triggered(bool)),
        this, SLOT(slotVenuePrevious()));

    m_venueNextAction = new QAction(QIcon(":/gfx/next.png"),
                    tr("Next cue"), this);
    connect(m_venueNextAction, SIGNAL(triggered(bool)),
        this, SLOT(slotVenueNext()));
}

void App::initVenueMenu()
{
    m_venueMenu = new QMenu(menuBar());
    m_venueMenu->setTitle(tr("Venue"));
    m_venueMenu->addAction(m_venueEditorAction);
    m_venueMenu->addAction(m_venueRenameAction);
    m_venueMenu->addSeparator();
    m_venueMenu->addAction(m_venueMonitorAction);
    m_venueMenu->addAction(m_venueTimeAction);
    m_venueMenu->addSeparator();
    m_venueMenu->addAction(m_venueRunAction);
    m_venueMenu->addAction(m_venuePauseAction);
    m_venueMenu->addAction(m_venueStopAction);
    m_venueMenu->addAction(m_venuePreviousAction);
    m_venueMenu->addAction(m_venueNextAction);
}

void App::slotVenueEditor()
{
    if (m_venueEditor == NULL)
    {
        QMdiSubWindow* sub = new QMdiSubWindow(centralWidget());
        m_venueEditor = new VenueEditor(sub, m_venue);

        sub->setWidget(m_venueEditor);
        sub->setAttribute(Qt::WA_DeleteOnClose);
        qobject_cast<QMdiArea*> (centralWidget())->addSubWindow(sub);
        m_venueEditor->show();
        sub->show();

        connect(m_venueEditor, SIGNAL(destroyed(QObject*)),
            this, SLOT(slotVenueEditorDestroyed()));
    }
}

void App::slotVenueEditorDestroyed()
{
    m_venueEditor = NULL;
}

void App::slotVenueMonitor()
{
    if (m_venueMonitor == NULL)
    {
        m_venueMonitorDock = new QDockWidget(this);
        m_venueMonitor = new VenueMonitor(m_venueMonitorDock, m_venue);

        m_venueMonitorDock->setWidget(m_venueMonitor);
        addDockWidget(Qt::RightDockWidgetArea, m_venueMonitorDock);

        connect(m_timer, SIGNAL(timeout()),
            m_venueMonitor, SLOT(slotMasterTick()));

        connect(m_venueMonitor, SIGNAL(destroyed(QObject*)),
            this, SLOT(slotVenueMonitorDestroyed()));
    }

    m_venueMonitorDock->show();
}

void App::slotVenueMonitorDestroyed()
{
    m_venueMonitor = NULL;
    m_venueMonitorDock = NULL;
}

void App::slotVenueTime()
{
    if (m_timeView == NULL)
    {
        QMdiSubWindow* sub = new QMdiSubWindow(centralWidget());
        m_timeView = new TimeView(sub);

        sub->setWidget(m_timeView);
        sub->setAttribute(Qt::WA_DeleteOnClose);
        qobject_cast<QMdiArea*> (centralWidget())->addSubWindow(sub);
        m_timeView->show();
        sub->show();

        connect(m_timer, SIGNAL(timeout()),
            m_timeView, SLOT(slotMasterTick()));

        connect(m_timeView, SIGNAL(destroyed(QObject*)),
            this, SLOT(slotVenueTimeDestroyed()));
    }
}

void App::slotVenueTimeDestroyed()
{
    m_timeView = NULL;
}

void App::slotVenueRename()
{
    QString name;
    bool ok = false;

    /* Get a new name for the new venue */
    name = QInputDialog::getText(centralWidget(), tr("Change Venue Name"),
                     tr("Venue name"), QLineEdit::Normal,
                     m_venue->name(), &ok);
    if (ok == true)
    {
        m_venue->setName(name);
        updateWindowTitle();
    }
}

void App::slotVenueRun()
{
    Q_ASSERT(m_venue != NULL);
    m_venue->setState(Venue::Running);
}

void App::slotVenuePause()
{
    Q_ASSERT(m_venue != NULL);
    m_venue->setState(Venue::Paused);
}

void App::slotVenueStop()
{
    int r;

    Q_ASSERT(m_venue != NULL);

    r = QMessageBox::question(this,
                  tr("Stop") + " " + m_venue->name() + "?",
                  tr("Do you really want to STOP the venue?"),
                  QMessageBox::Yes, QMessageBox::No);
    if (r == QMessageBox::Yes)
        m_venue->setState(Venue::Stopped);
}

void App::slotVenuePrevious()
{
    Q_ASSERT(m_venue != NULL);
    m_venue->skipToPreviousCue();
}

void App::slotVenueNext()
{
    Q_ASSERT(m_venue != NULL);
    m_venue->skipToNextCue();
}

/*****************************************************************************
 * Stage actions
 *****************************************************************************/

void App::initStageActions()
{
    m_stageShowAction = new QAction(QIcon(":/gfx/stage.png"),
                    tr("Show Stage View"), this);
    m_stageShowAction->setShortcut(QKeySequence(Qt::Key_F5));
    connect(m_stageShowAction, SIGNAL(triggered(bool)),
        this, SLOT(slotStageShow()));

    m_stageHideAction = new QAction(tr("Hide Stage View"), this);
    connect(m_stageHideAction, SIGNAL(triggered(bool)),
        this, SLOT(slotStageHide()));

    m_stageMonitorAction = new QAction(QIcon(":/gfx/stagemonitor.png"),
                       tr("Stage Monitor"), this);
    connect(m_stageMonitorAction, SIGNAL(triggered(bool)),
        this, SLOT(slotStageMonitor()));

    m_stageViewEditorAction = new QAction(QIcon(":/gfx/configure.png"),
                          tr("Configure Stage View"), this);
    connect(m_stageViewEditorAction, SIGNAL(triggered(bool)),
        this, SLOT(slotStageViewEditor()));
}

void App::initStageMenu()
{
    m_stageMenu = new QMenu(menuBar());
    m_stageMenu->setTitle(tr("Stage"));
    m_stageMenu->addAction(m_stageShowAction);
    m_stageMenu->addAction(m_stageHideAction);
    m_stageMenu->addSeparator();
    m_stageMenu->addAction(m_stageViewEditorAction);
    m_stageMenu->addSeparator();
    m_stageMenu->addAction(m_stageMonitorAction);
}

void App::slotStageShow()
{
    if (m_stageView == NULL)
    {
        QDesktopWidget* dw;
        QRect rect;
        int screen;

        /* Find out the last available screen and display the stage view in it 
          TODO: Give the user an option for screen in case there's more than two
          screens... */
        dw = QApplication::desktop();
        screen = dw->numScreens() - 1;
        rect = dw->screenGeometry(screen);

        m_stageView = new StageView(dw->screen(screen), m_venue,
                        Qt::FramelessWindowHint |
                        Qt::WindowSystemMenuHint);

        m_stageView->setWindowTitle(KApplicationName + " - " +
                        tr("Stage View"));
        m_stageView->setWindowIcon(QIcon(":/gfx/stage.png"));
        m_stageView->setProperties(m_venue->stageViewProperties());

        /* Resize the window to fill the whole screen */
        m_stageView->setGeometry(rect);
        m_stageView->showMultiHead();

        connect(m_timer, SIGNAL(timeout()),
            m_stageView, SLOT(slotMasterTick()));

        m_stageView->setAttribute(Qt::WA_DeleteOnClose);
        connect(m_stageView, SIGNAL(destroyed(QObject*)),
            this, SLOT(slotStageViewDestroyed()));

        m_stageShowAction->setEnabled(false);
        m_stageHideAction->setEnabled(true);
    }
}

void App::slotStageViewDestroyed()
{
    m_stageView = NULL;

    m_stageShowAction->setEnabled(true);
    m_stageHideAction->setEnabled(false);
}

void App::slotStageHide()
{
    if (m_stageView != NULL)
        m_stageView->close();
}

void App::slotStageMonitor()
{
    if (m_stageMonitor == NULL)
    {
        QMdiSubWindow* sub = new QMdiSubWindow(centralWidget());
        m_stageMonitor = new StageView(sub, m_venue);
        m_stageMonitor->setProperties(m_venue->stageViewProperties());

        sub->setWindowTitle(tr("Stage Monitor"));
        sub->setWindowIcon(QIcon(":/gfx/stagemonitor.png"));

        sub->setWidget(m_stageMonitor);
        sub->setAttribute(Qt::WA_DeleteOnClose);
        qobject_cast<QMdiArea*> (centralWidget())->addSubWindow(sub);
        m_stageMonitor->show();
        sub->show();

        connect(m_timer, SIGNAL(timeout()),
            m_stageMonitor, SLOT(slotMasterTick()));

        connect(m_stageMonitor, SIGNAL(destroyed(QObject*)),
            this, SLOT(slotStageMonitorDestroyed()));
    }
}

void App::slotStageMonitorDestroyed()
{
    m_stageMonitor = NULL;
}

void App::slotStageViewEditor()
{
    StageViewEditor sve(this, m_venue->stageViewProperties());
    if (sve.exec() == QDialog::Accepted)
    {
        m_venue->setStageViewProperties(sve.properties());

        if (m_stageView != NULL)
            m_stageView->setProperties(sve.properties());
        if (m_stageMonitor != NULL)
            m_stageMonitor->setProperties(sve.properties());
    }
}

/*****************************************************************************
 * Window actions
 *****************************************************************************/

void App::initWindowMenu()
{
    m_windowMenu = new QMenu(menuBar());
    m_windowMenu->setTitle(tr("Window"));

    connect(m_windowMenu, SIGNAL(aboutToShow()),
        this, SLOT(slotWindowAboutToShow()));

    connect(m_windowMenu, SIGNAL(triggered(QAction*)),
        this, SLOT(slotWindowTriggered(QAction*)));
}

void App::slotWindowAboutToShow()
{
    int children = 0;

    Q_ASSERT(m_windowMenu != NULL);
    m_windowMenu->clear();

    /* Fill window menu with workspace's child windows */
    QListIterator <QMdiSubWindow*>
        it(qobject_cast<QMdiArea*> (centralWidget())->subWindowList());
    while (it.hasNext() == true)
    {
        QMdiSubWindow* window = it.next();
        QAction* action = new QAction(window->windowIcon(),
                          window->windowTitle(),
                          m_windowMenu);
        /* Store the child widget's index within the subwindow's list
           as the item's data. This can be used to get the same child
           widget in slotWindowTriggered() */
        action->setData(children);
        m_windowMenu->addAction(action);

        children++;
    }

    if (children > 0)
        m_windowMenu->addSeparator();

    m_windowMenu->addAction(QIcon(":/gfx/windowcascade.png"), tr("Cascade"),
                this, SLOT(slotWindowCascade()));
    m_windowMenu->addAction(QIcon(":/gfx/windowtile.png"), tr("Tile"),
                this, SLOT(slotWindowTile()));
}

void App::slotWindowTriggered(QAction* action)
{
    QMdiSubWindow* window;
    bool ok = false;
    int index = 0;

    Q_ASSERT(action != NULL);

    index = action->data().toInt(&ok);
    if (ok == true)
    {
            window = qobject_cast<QMdiArea*> 
          (centralWidget())->subWindowList().at(index);
        if (window != NULL)
        {
                window->raise();
            window->showNormal();
        }
    }
}

void App::slotWindowCascade()
{
    qobject_cast<QMdiArea*> (centralWidget())->cascadeSubWindows();
}

void App::slotWindowTile()
{
    qobject_cast<QMdiArea*> (centralWidget())->tileSubWindows();
}

/*****************************************************************************
 * Help menu slots
 *****************************************************************************/

void App::initHelpActions()
{
    m_helpIndexAction = new QAction(QIcon(":/gfx/help.png"), tr("Help"),
                    this);
    m_helpIndexAction->setShortcut(QKeySequence(Qt::SHIFT + Qt::Key_F1));
    connect(m_helpIndexAction, SIGNAL(triggered(bool)),
        this, SLOT(slotHelpIndex()));

    m_helpAboutAction = new QAction(QIcon(":/gfx/stagemanager.png"),
                    tr("About %1") .arg(KApplicationName),
                    this);
    connect(m_helpAboutAction, SIGNAL(triggered(bool)),
        this, SLOT(slotHelpAbout()));

    m_helpAboutQTAction = new QAction(QIcon(":/gfx/aboutQT.png"),
                      tr("About QT"), this);
    connect(m_helpAboutQTAction, SIGNAL(triggered(bool)),
        this, SLOT(slotHelpAboutQT()));
}

void App::initHelpMenu()
{
    m_helpMenu = new QMenu(menuBar());
    m_helpMenu->setTitle(tr("Help"));
    m_helpMenu->addAction(m_helpIndexAction);
    m_helpMenu->addSeparator();
    m_helpMenu->addAction(m_helpAboutAction);
    m_helpMenu->addAction(m_helpAboutQTAction);
}

void App::slotHelpIndex()
{
    HelpBrowser* browser = new HelpBrowser(NULL);
    browser->setAttribute(Qt::WA_DeleteOnClose);
    browser->show();
}

void App::slotHelpAbout()
{
    AboutBox ab(this);
    ab.exec();
}

void App::slotHelpAboutQT()
{
    QMessageBox::aboutQt(this, KApplicationName);
}

/*****************************************************************************
 * Menu- and toolbar
 *****************************************************************************/

void App::initActions()
{
    initFileActions();
    initVenueActions();
    initStageActions();
    initHelpActions();
}

void App::initToolBar()
{
    m_toolBar = new QToolBar(KApplicationName, this);
    addToolBar(m_toolBar);
    m_toolBar->setMovable(false);

    m_toolBar->addAction(m_fileNewAction);
    m_toolBar->addAction(m_fileOpenAction);
    m_toolBar->addAction(m_fileSaveAction);
    m_toolBar->addSeparator();
    m_toolBar->addAction(m_venueMonitorAction);
    m_toolBar->addAction(m_venueTimeAction);
    m_toolBar->addAction(m_stageShowAction);
    m_toolBar->addSeparator();
    m_toolBar->addAction(m_venueRunAction);
    m_toolBar->addAction(m_venuePauseAction);
    m_toolBar->addAction(m_venueStopAction);
    m_toolBar->addAction(m_venuePreviousAction);
    m_toolBar->addAction(m_venueNextAction);
}

void App::initMenuBar()
{
    initFileMenu();
    initVenueMenu();
    initStageMenu();
    initWindowMenu();
    initHelpMenu();

    menuBar()->addMenu(m_fileMenu);
    menuBar()->addMenu(m_venueMenu);
    menuBar()->addMenu(m_stageMenu);
    menuBar()->addMenu(m_windowMenu);
    menuBar()->addSeparator();
    menuBar()->addMenu(m_helpMenu);
}

/*****************************************************************************
 * Generic utilities
 *****************************************************************************/

int App::timeToSecs(const QTime& time)
{
    int secs = time.hour() * 3600;
    secs += time.minute() * 60;
    secs += time.second();
    return secs;
}

QTime App::secsToTime(int secs)
{
    QTime time(0, 0);
    return time.addSecs(secs);
}
