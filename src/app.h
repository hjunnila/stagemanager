/*
  StageManager
  app.h

  Copyright (C) Heikki Junnila

  This program is free software; you can redistribute it and/or
  modify it under the terms of the GNU General Public License
  Version 2 as published by the Free Software Foundation.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details. The license is
  in the file "COPYING".

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*/

#ifndef APP_H
#define APP_H

#include <QMainWindow>
#include <QMdiArea>

#include "venue.h"

#define KApplicationName QString("Stage Manager")
#define KApplicationVersion QString("Version 1.0")

#define KExtVenue ".smv"

#define KXMLCreator "Creator"
#define KXMLCreatorName "Name"
#define KXMLCreatorVersion "Version"
#define KXMLCreatorAuthor "Author"

class QDockWidget;
class QToolButton;
class QCloseEvent;
class QStatusBar;
class QMenuBar;
class QToolBar;
class QAction;
class QTimer;
class QMenu;

class VenueEditor;
class VenueMonitor;
class StageView;
class TimeView;

class App : public QMainWindow
{
    Q_OBJECT

    /*********************************************************************
     * Initialization
     *********************************************************************/
public:
    App(QWidget* parent = 0);
    ~App();

private:
    Q_DISABLE_COPY(App)

protected:
    /** Load default settings for the application */
    void loadDefaults();

    /** Save default settings for the application */
    void saveDefaults();

protected:
    /** Hook for closing the application window thru window manager */
    void closeEvent(QCloseEvent* e);

    /*********************************************************************
     * Venue
     *********************************************************************/
public:
    /** Get the current venue */
    Venue* venue() const;

    /** Start from scratch (also deletes an existing venue) */
    void newVenue();

protected:
    /** Set the current venue (deletes the old one as well) */
    void setVenue(Venue* venue);

    /** Update the window title according to the current venue (if any) */
    void updateWindowTitle();

protected slots:
    /** Slot that receives Venue modified status changes */
    void slotVenueModified(bool modified);

    /** Slot that received Venue state changes */
    void slotVenueStateChanged(Venue::State state);

protected:
    /** The current venue */
    Venue* m_venue;

    /*********************************************************************
     * Master timer
     *********************************************************************/
protected:
    /** Start the master timer that drives everything in StageManager */
    void startMasterTimer();

    /** Stop the master timer */
    void stopMasterTimer();

protected slots:
    /** Timeout that is called every second while the master timer runs */
    void slotMasterTimeout();

signals:
    /** A proxy signal for the master timer ticks. */
    void masterTick();

protected:
    /** The master timer */
    QTimer* m_timer;

    /*********************************************************************
     * File actions
     *********************************************************************/
protected:
    void initFileActions();
    void initFileMenu();

protected slots:
    void slotFileNew();
    void slotFileOpen();
    void slotFileSave();
    void slotFileSaveAs();
    void slotFileQuit();

protected:
    QMenu* m_fileMenu;

    QAction* m_fileNewAction;
    QAction* m_fileOpenAction;
    QAction* m_fileSaveAction;
    QAction* m_fileSaveAsAction;
    QAction* m_fileQuitAction;

    /*********************************************************************
     * Venue actions
     *********************************************************************/
protected:
    void initVenueActions();
    void initVenueMenu();

protected slots:
    void slotVenueEditor();
    void slotVenueEditorDestroyed();

    void slotVenueMonitor();
    void slotVenueMonitorDestroyed();

    void slotVenueTime();
    void slotVenueTimeDestroyed();

    void slotVenueRename();

    void slotVenueRun();
    void slotVenuePause();
    void slotVenueStop();

    void slotVenuePrevious();
    void slotVenueNext();

protected:
    QMenu* m_venueMenu;

    QAction* m_venueEditorAction;
    QAction* m_venueMonitorAction;
    QAction* m_venueTimeAction;

    QAction* m_venueRenameAction;

    QAction* m_venueRunAction;
    QAction* m_venuePauseAction;
    QAction* m_venueStopAction;

    QAction* m_venuePreviousAction;
    QAction* m_venueNextAction;

protected:
    VenueEditor* m_venueEditor;
    VenueMonitor* m_venueMonitor;
    QDockWidget* m_venueMonitorDock;
    TimeView* m_timeView;

    /*********************************************************************
     * Stage menu
     *********************************************************************/
protected:
    void initStageActions();
    void initStageMenu();

protected slots:
    void slotStageShow();
    void slotStageViewDestroyed();
    void slotStageHide();
    void slotStageViewEditor();

    void slotStageMonitor();
    void slotStageMonitorDestroyed();

protected:
    QMenu* m_stageMenu;

    QAction* m_stageShowAction;
    QAction* m_stageHideAction;
    QAction* m_stageViewEditorAction;
    QAction* m_stageMonitorAction;

protected:
    StageView* m_stageView;
    StageView* m_stageMonitor;

    /*********************************************************************
     * Window menu
     *********************************************************************/
protected:
    void initWindowMenu();

protected slots:
    void slotWindowCascade();
    void slotWindowTile();

    void slotWindowAboutToShow();
    void slotWindowTriggered(QAction* action);

protected:
    QMenu* m_windowMenu;

    /*********************************************************************
     * Help menu slots
     *********************************************************************/
protected:
    void initHelpActions();
    void initHelpMenu();

protected slots:
    void slotHelpIndex();
    void slotHelpAbout();
    void slotHelpAboutQT();

protected:
    QMenu* m_helpMenu;

    QAction* m_helpIndexAction;
    QAction* m_helpAboutAction;
    QAction* m_helpAboutQTAction;

    /*********************************************************************
     * Menu- and toolbar
     *********************************************************************/
protected:
    void initActions();
    void initMenuBar();
    void initToolBar();

protected:
    QToolBar* m_toolBar;

    /*********************************************************************
     * Generic utilities
     *********************************************************************/
public:
    static int timeToSecs(const QTime& time);
    static QTime secsToTime(int secs);
};

#endif
