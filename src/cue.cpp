/*
  StageManager
  cue.cpp

  Copyright (C) Heikki Junnila

  This program is free software; you can redistribute it and/or
  modify it under the terms of the GNU General Public License
  Version 2 as published by the Free Software Foundation.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details. The license is
  in the file "COPYING".

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*/

#include <QDebug>
#include <QtXml>

#include "venue.h"
#include "cue.h"

/*****************************************************************************
 * Initialization
 *****************************************************************************/

Cue::Cue(QObject* parent) : QObject(parent)
{
    m_name = QString::null;
    m_duration = 0;
    m_waitTime = 0;
    m_timeLeft = 0;
}

Cue::~Cue()
{
}

/*****************************************************************************
 * Name
 *****************************************************************************/

void Cue::setName(const QString& name)
{
    m_name = name;
    emit modified(true);
}

QString Cue::name() const
{
    return m_name;
}

/*****************************************************************************
 * Time
 *****************************************************************************/

void Cue::setDuration(int seconds)
{
    m_duration = seconds;

    /* Initially the cue has 'duration' amount of time left */
    setTimeLeft(seconds);
    emit modified(true);
}

int Cue::duration() const
{
    return m_duration;
}

void Cue::setWaitTime(int seconds)
{
    m_waitTime = seconds;
}

int Cue::waitTime() const
{
    return m_waitTime;
}

void Cue::setTimeLeft(int seconds)
{
    m_timeLeft = seconds;
}

int Cue::timeLeft() const
{
    return m_timeLeft;
}

/*****************************************************************************
 * Load & Save
 *****************************************************************************/

bool Cue::save(QDomDocument* doc, QDomElement* venue_root) const
{
    QDomElement root;
    QDomText text;
    QString str;

    Q_ASSERT(doc != NULL);
    Q_ASSERT(venue_root != NULL);

    /* Cue entry */
    root = doc->createElement(KXMLCue);
    venue_root->appendChild(root);

    /* Duration as an attribute */
    str.setNum(duration());
    root.setAttribute(KXMLCueDuration, str);

    /* Cue name as the tag text */
    text = doc->createTextNode(name());
    root.appendChild(text);

    return true;
}

bool Cue::load(const QDomElement& root)
{
    if (root.tagName() != KXMLCue)
    {
        qWarning() << "Cue node not found!";
        return false;
    }

    /* Duration */
    setDuration(root.attribute(KXMLCueDuration).toInt());

    /* Cue name */
    setName(root.text());

    return true;
}
