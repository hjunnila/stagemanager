/*
  StageManager
  cue.h

  Copyright (C) Heikki Junnila

  This program is free software; you can redistribute it and/or
  modify it under the terms of the GNU General Public License
  Version 2 as published by the Free Software Foundation.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details. The license is
  in the file "COPYING".

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*/

#ifndef CUE_H
#define CUE_H

#include <QObject>

#define KXMLCue "Cue"
#define KXMLCueDuration "Duration"

class QDomDocument;
class QDomElement;

class Cue : public QObject
{
    Q_OBJECT
    Q_DISABLE_COPY(Cue)

    /*********************************************************************
     * Initialization
     *********************************************************************/
public:
    Cue(QObject* parent);
    ~Cue();

    /*********************************************************************
     * Dynamic attributes
     *********************************************************************/
public:
    /** Set this cue's name */
    void setName(const QString& name);

    /** Get the name of this cue */
    QString name() const;

    /** Set the amount of seconds allocated for this cue */
    void setDuration(int seconds);

    /** Get the amount of seconds allocated for this cue */
    int duration() const;

signals:
    /** Emitted when a dynamic attribute changes */
    void modified(bool status);

private:
    QString m_name;
    int m_duration;

    /*********************************************************************
     * Run-time attributes
     *********************************************************************/
public:
    /** Get this cue's waiting time i.e. how many seconds must pass
        until this cue is triggered. */
    void setWaitTime(int seconds);

    /** Get this cue's waiting time i.e. how many seconds must pass
        until this cue is triggered. */
    int waitTime() const;

    /** Set the number of seconds left for this cue in this venue i.e.
        until the next cue is triggered */
    void setTimeLeft(int seconds);

    /** Get the number of seconds left for this cue in this venue i.e.
        until the next cue is triggered */
    int timeLeft() const;

private:
    int m_waitTime;
    int m_timeLeft;

    /*********************************************************************
     * Load & Save
     *********************************************************************/
public:
    /** Save this cue into the given XML document, under venue_root */
    bool save(QDomDocument* doc, QDomElement* venue_root) const;

    /** Load this cue's contents from the given XML subtree element */
    bool load(const QDomElement& cue_root);
};

#endif
