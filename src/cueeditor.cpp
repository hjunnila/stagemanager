/*
  StageManager
  cueeditor.cpp

  Copyright (C) Heikki Junnila

  This program is free software; you can redistribute it and/or
  modify it under the terms of the GNU General Public License
  Version 2 as published by the Free Software Foundation.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details. The license is
  in the file "COPYING".

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*/

#include <QDialogButtonBox>
#include <QStringList>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QPushButton>
#include <QSpacerItem>
#include <QLineEdit>
#include <QComboBox>
#include <QTimeEdit>
#include <QLabel>

#include "cueeditor.h"
#include "venue.h"
#include "cue.h"
#include "app.h"

CueEditor::CueEditor(QWidget* parent, Cue* cue, Venue* venue) : QDialog(parent)
{
    m_cue = cue;
    Q_ASSERT(cue != NULL);

    m_venue = venue;
    Q_ASSERT(venue != NULL);

    setWindowTitle(tr("%1 - Edit Cue") .arg(venue->name()));

    new QVBoxLayout(this);
    layout()->setSpacing(10);

    /* Cue */
    QHBoxLayout* hbox = new QHBoxLayout();
    layout()->addItem(hbox);
    hbox->setSpacing(10);

    m_cueLabel = new QLabel(this);
    hbox->addWidget(m_cueLabel);
    m_cueLabel->setText(tr("Cue"));

    m_cueCombo = new QComboBox(this);
    hbox->addWidget(m_cueCombo);
    m_cueCombo->setEditable(true);
    m_cueCombo->addItems(m_venue->cueNames());
    m_cueCombo->setCurrentIndex(m_cueCombo->findText(cue->name()));

    /* Duration */
    hbox = new QHBoxLayout();
    layout()->addItem(hbox);
    hbox->setSpacing(10);

    m_durationLabel = new QLabel(this);
    hbox->addWidget(m_durationLabel);
    m_durationLabel->setText(tr("Duration"));

    m_durationEdit = new QTimeEdit(this);
    hbox->addWidget(m_durationEdit);
    m_durationEdit->setTime(App::secsToTime(cue->duration()));
    m_durationEdit->setDisplayFormat("hh:mm:ss");
    qDebug() << "dur" << App::secsToTime(cue->duration());

    /* Dialog buttons */
    m_buttonBox = new QDialogButtonBox(QDialogButtonBox::Ok |
                       QDialogButtonBox::Cancel,
                       Qt::Horizontal, this);
    layout()->addWidget(m_buttonBox);
    connect(m_buttonBox, SIGNAL(accepted()), this, SLOT(accept()));
    connect(m_buttonBox, SIGNAL(rejected()), this, SLOT(reject()));

    resize(300, 100);
}

CueEditor::~CueEditor()
{
}

void CueEditor::accept()
{
    Q_ASSERT(m_cue != NULL);

    /* Store the cue name so that it can be selected again later */
    m_venue->addCueName(m_cueCombo->currentText());

    /* Set cue name */
    m_cue->setName(m_cueCombo->currentText());

    /* Set cue duration */
    m_cue->setDuration(App::timeToSecs(m_durationEdit->time()));

    QDialog::accept();
}
