/*
  StageManager
  cueeditor.h

  Copyright (C) Heikki Junnila

  This program is free software; you can redistribute it and/or
  modify it under the terms of the GNU General Public License
  Version 2 as published by the Free Software Foundation.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details. The license is
  in the file "COPYING".

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*/

#ifndef CUEEDITOR_H
#define CUEEDITOR_H

#include <QDialog>

class Cue;
class Venue;

class QLabel;
class QComboBox;
class QTimeEdit;
class QDialogButtonBox;

class CueEditor : public QDialog
{
    Q_OBJECT
    Q_DISABLE_COPY(CueEditor)

public:
    CueEditor(QWidget* parent, Cue* cue, Venue* venue);
    ~CueEditor();

public slots:
    void accept();

private:
    Cue* m_cue;
    Venue* m_venue;

    QLabel* m_cueLabel;
    QComboBox* m_cueCombo;

    QLabel* m_durationLabel;
    QTimeEdit* m_durationEdit;

    QDialogButtonBox* m_buttonBox;
};

#endif
