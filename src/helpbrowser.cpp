/*
  Stage Manager
  helpbrowser.cpp

  Copyright (C) Heikki Junnila

  This program is free software; you can redistribute it and/or
  modify it under the terms of the GNU General Public License
  Version 2 as published by the Free Software Foundation.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details. The license is
  in the file "COPYING".

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*/

#include <QTextBrowser>
#include <QCloseEvent>
#include <QVBoxLayout>
#include <QToolBar>
#include <QAction>
#include <QUrl>

#include "app.h"
#include "helpbrowser.h"

HelpBrowser::HelpBrowser(QWidget* parent) : QWidget(parent)
{
    initActions();
    initView();
}

HelpBrowser::~HelpBrowser()
{
}

void HelpBrowser::initActions()
{
    m_prevAction = new QAction(QIcon(":/gfx/previous.png"),
                   tr("Previous page"), this);
    connect(m_prevAction, SIGNAL(triggered(bool)), this, SLOT(slotPrev()));

    m_nextAction = new QAction(QIcon(":/gfx/next.png"),
                   tr("Next page"), this);
    connect(m_nextAction, SIGNAL(triggered(bool)), this, SLOT(slotNext()));
}

void HelpBrowser::initView()
{
    setWindowTitle(tr("%1 - Document Browser") .arg(KApplicationName));
    resize(600, 600);

    new QVBoxLayout(this);

    /* Toolbar */
    m_toolbar = new QToolBar(tr("Document Browser"), this);
    layout()->addWidget(m_toolbar);

    m_toolbar->addAction(m_prevAction);
    m_toolbar->addAction(m_nextAction);

    /* Browser */
    m_browser = new QTextBrowser(this);
    layout()->addWidget(m_browser);

    connect(m_browser, SIGNAL(backwardAvailable(bool)),
        this, SLOT(slotBackwardAvailable(bool)));
    connect(m_browser, SIGNAL(forwardAvailable(bool)),
        this, SLOT(slotForwardAvailable(bool)));

    m_browser->setSource(QUrl("documentation/index.html"));
}

void HelpBrowser::slotPrev()
{
    m_browser->backward();
}

void HelpBrowser::slotBackwardAvailable(bool available)
{
    m_prevAction->setEnabled(available);
}

void HelpBrowser::slotNext()
{
    m_browser->forward();
}

void HelpBrowser::slotForwardAvailable(bool available)
{
    m_nextAction->setEnabled(available);
}

void HelpBrowser::closeEvent(QCloseEvent*)
{
    emit closed();
}
