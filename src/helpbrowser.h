/*
  Stage Manager
  helpbrowser.h

  Copyright (C) Heikki Junnila

  This program is free software; you can redistribute it and/or
  modify it under the terms of the GNU General Public License
  Version 2 as published by the Free Software Foundation.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details. The license is
  in the file "COPYING".

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*/

#ifndef HELPBROWSER_H
#define HELPBROWSER_H

#include <QWidget>

class QTextBrowser;
class QCloseEvent;
class QToolBar;
class QAction;

class HelpBrowser : public QWidget
{
    Q_OBJECT

public:
    HelpBrowser(QWidget* parent);
    ~HelpBrowser();

private:
    Q_DISABLE_COPY(HelpBrowser)

protected:
    void initActions();
    void initView();
    void updateButtons();

protected slots:
    void slotPrev();
    void slotBackwardAvailable(bool);
    void slotNext();
    void slotForwardAvailable(bool);

protected:
    void closeEvent(QCloseEvent*);

signals:
    void closed();

protected:
    QToolBar* m_toolbar;
    QTextBrowser* m_browser;

    QAction* m_prevAction;
    QAction* m_nextAction;
};

#endif
