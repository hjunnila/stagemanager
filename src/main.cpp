/*
  StageManager
  main.cpp

  Copyright (C) Heikki Junnila

  This program is free software; you can redistribute it and/or
  modify it under the terms of the GNU General Public License
  Version 2 as published by the Free Software Foundation.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details. The license is
  in the file "COPYING".

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*/

#include <QApplication>
#include <QTranslator>
#include <QLocale>
#include <QString>

#include "app.h"

int main(int argc, char **argv)
{
    QApplication qapp(argc, argv);

#ifdef __APPLE__
        /* Load plugins from within the bundle ONLY */
        QDir dir(QApplication::applicationDirPath());
        dir.cdUp();
        dir.cd("Plugins");
        QApplication::setLibraryPaths(QStringList(dir.absolutePath()));
#endif

    QTranslator tran;
    QString trfile = QString("stagemanager_%1").arg(QLocale::system().name());
    if (tran.load(trfile, DATADIR) == true)
        qapp.installTranslator(&tran);

    App app(NULL);
    app.show();

    return qapp.exec();
}
