TEMPLATE	= app
LANGUAGE	= C++
TARGET		= stagemanager

QMAKE_CXXFLAGS	+= -Werror
CONFIG		+= qt
CONFIG		-= app_bundle
QT		+= core gui xml widgets

include(../variables.pri)

DEFINES		+= DATADIR=\\\"$$DATADIR\\\"
DEFINES		+= DOCSDIR=\\\"$$DOCSDIR\\\"

RESOURCES 	= stagemanager.qrc
win32:RC_FILE	= stagemanager.rc

HEADERS	+= \
	aboutbox.h \
	app.h \
	cue.h \
	cueeditor.h \
	helpbrowser.h \
	stageview.h \
	stagevieweditor.h \
	stageviewproperties.h \
	timeview.h \
	venue.h \
	venueeditor.h \
	venuemonitor.h

SOURCES	+= \
	aboutbox.cpp \
	app.cpp \
	cue.cpp \
	cueeditor.cpp \
	helpbrowser.cpp \
	main.cpp \
	stageview.cpp \
	stagevieweditor.cpp \
	stageviewproperties.cpp \
	timeview.cpp \
	venue.cpp \
	venueeditor.cpp \
	venuemonitor.cpp

FORMS = \
	aboutbox.ui \
	stagevieweditor.ui

TRANSLATIONS = \
	stagemanager_fi_FI.ts

#############################################################################
# Translations
#############################################################################

isEmpty(QMAKE_LRELEASE) {
	win32:QMAKE_LRELEASE = $$[QT_INSTALL_BINS]\lrelease.exe
	else:QMAKE_LRELEASE = $$[QT_INSTALL_BINS]/lrelease
}

updateqm.input = TRANSLATIONS
updateqm.output = ${QMAKE_FILE_BASE}.qm
updateqm.commands = $$QMAKE_LRELEASE ${QMAKE_FILE_IN} -qm ${QMAKE_FILE_BASE}.qm
updateqm.CONFIG += no_link target_predeps
QMAKE_EXTRA_COMPILERS += updateqm

#############################################################################
# Installation
#############################################################################

macx {
	# This must be after "TARGET = " and before target installation so that
	# install_name_tool can be run before target installation
	include(../macx/nametool.pri)
}

target.path = $$BINDIR
INSTALLS += target
