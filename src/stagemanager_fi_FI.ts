<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="fi_FI">
<context>
    <name>AboutBox</name>
    <message>
        <location filename="aboutbox.ui" line="13"/>
        <source>About Stage Manager</source>
        <translation>Tietoja Stage Managerista</translation>
    </message>
    <message>
        <location filename="aboutbox.ui" line="88"/>
        <source>This application is licensed under the GNU GPL version 2.</source>
        <translation>Tämä sovellus on lisensoitu GNU GPL versio 2:n alaisena.</translation>
    </message>
    <message>
        <location filename="aboutbox.ui" line="97"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Arial&apos;; font-size:9pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p align=&quot;center&quot; style=&quot; margin-top:18px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:14pt; font-weight:600;&quot;&gt;GNU GENERAL PUBLIC LICENSE&lt;/span&gt;&lt;/p&gt;
&lt;p align=&quot;center&quot; style=&quot; margin-top:18px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:14pt; font-weight:600;&quot;&gt;&lt;span style=&quot; font-size:xx-large;&quot;&gt;Version 2, June 1991&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Copyright (C) 1989, 1991 Free Software Foundation, Inc. 675 Mass Ave, Cambridge, MA 02139, USAEveryone is permitted to copy and distribute verbatim copies of this license document, but changing it is not allowed.&lt;/p&gt;
&lt;p align=&quot;center&quot; style=&quot; margin-top:16px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:x-large; font-weight:600;&quot;&gt;&lt;span style=&quot; font-size:x-large;&quot;&gt;Preamble&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;The licenses for most software are designed to take away your freedom to share and change it.  By contrast, the GNU General Public License is intended to guarantee your freedom to share and change free software--to make sure the software is free for all its users.  This General Public License applies to most of the Free Software Foundation&apos;s software and to any other program whose authors commit tousing it.  (Some other Free Software Foundation software is covered by the GNU Library General Public License instead.)  You can apply it to your programs, too.&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;When we speak of free software, we are referring to freedom, not price.  Our General Public Licenses are designed to make sure that you have the freedom to distribute copies of free software (and charge for this service if you wish), that you receive source code or can get it if you want it, that you can change the software or use pieces of it in new free programs; and that you know you can do these things.&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;To protect your rights, we need to make restrictions that forbid anyone to deny you these rights or to ask you to surrender the rights. These restrictions translate to certain responsibilities for you if youdistribute copies of the software, or if you modify it.&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;For example, if you distribute copies of such a program, whether gratis or for a fee, you must give the recipients all the rights that you have.  You must make sure that they, too, receive or can get thesource code.  And you must show them these terms so they know their rights.&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt; We protect your rights with two steps: (1) copyright the software, and (2) offer you this license which gives you legal permission to copy, distribute and/or modify the software.&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Also, for each author&apos;s protection and ours, we want to make certain that everyone understands that there is no warranty for this free software.  If the software is modified by someone else and passed on, we want its recipients to know that what they have is not the original, so that any problems introduced by others will not reflect on the original authors&apos; reputations.&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Finally, any free program is threatened constantly by software patents.  We wish to avoid the danger that redistributors of a free program will individually obtain patent licenses, in effect making the program proprietary.  To prevent this, we have made it clear that any patent must be licensed for everyone&apos;s free use or not licensed at all.&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;The precise terms and conditions for copying, distribution and modification follow.&lt;/p&gt;
&lt;p align=&quot;center&quot; style=&quot; margin-top:18px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:xx-large; font-weight:600;&quot;&gt;&lt;span style=&quot; font-size:xx-large;&quot;&gt;GNU GENERAL PUBLIC LICENSE TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;0. This License applies to any program or other work which containsa notice placed by the copyright holder saying it may be distributedunder the terms of this General Public License.  The &quot;Program&quot;, below,refers to any such program or work, and a &quot;work based on the Program&quot;means either the Program or any derivative work under copyright law:that is to say, a work containing the Program or a portion of it,either verbatim or with modifications and/or translated into anotherlanguage.  (Hereinafter, translation is included without limitation inthe term &quot;modification&quot;.)  Each licensee is addressed as &quot;you&quot;.&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Activities other than copying, distribution and modification are notcovered by this License; they are outside its scope.  The act ofrunning the Program is not restricted, and the output from the Programis covered only if its contents constitute a work based on theProgram (independent of having been made by running the Program).Whether that is true depends on what the Program does.&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;1. You may copy and distribute verbatim copies of the Program&apos;ssource code as you receive it, in any medium, provided that youconspicuously and appropriately publish on each copy an appropriatecopyright notice and disclaimer of warranty; keep intact all thenotices that refer to this License and to the absence of any warranty;and give any other recipients of the Program a copy of this Licensealong with the Program.&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;You may charge a fee for the physical act of transferring a copy, andyou may at your option offer warranty protection in exchange for a fee.&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;2. You may modify your copy or copies of the Program or any portionof it, thus forming a work based on the Program, and copy anddistribute such modifications or work under the terms of Section 1above, provided that you also meet all of these conditions:&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;a) You must cause the modified files to carry prominent notices stating that you changed the files and the date of any change.&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;b) You must cause any work that you distribute or publish, that in whole or in part contains or is derived from the Program or any part thereof, to be licensed as a whole at no charge to all third parties under the terms of this License.&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;c) If the modified program normally reads commands interactively when run, you must cause it, when started running for such interactive use in the most ordinary way, to print or display an announcement including an appropriate copyright notice and a notice that there is no warranty (or else, saying that you provide    a warranty) and that users may redistribute the program under these conditions, and telling the user how to view a copy of this License. (Exception: if the Program itself is interactive but does not normally print such an announcement, your work based on the Program is not required to print an announcement.)&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;These requirements apply to the modified work as a whole.  Ifidentifiable sections of that work are not derived from the Program,and can be reasonably considered independent and separate works inthemselves, then this License, and its terms, do not apply to thosesections when you distribute them as separate works.  But when youdistribute the same sections as part of a whole which is a work basedon the Program, the distribution of the whole must be on the terms ofthis License, whose permissions for other licensees extend to theentire whole, and thus to each and every part regardless of who wrote it.&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Thus, it is not the intent of this section to claim rights or contestyour rights to work written entirely by you; rather, the intent is toexercise the right to control the distribution of derivative orcollective works based on the Program.&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;In addition, mere aggregation of another work not based on the Programwith the Program (or with a work based on the Program) on a volume ofa storage or distribution medium does not bring the other work underthe scope of this License.&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;3. You may copy and distribute the Program (or a work based on it,under Section 2) in object code or executable form under the terms ofSections 1 and 2 above provided that you also do one of the following:&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;a) Accompany it with the complete corresponding machine-readable source code, which must be distributed under the terms of Sections 1 and 2 above on a medium customarily used for software interchange; or, &lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;b) Accompany it with a written offer, valid for at least three years, to give any third party, for a charge no more than your cost of physically performing source distribution, a complete machine-readable copy of the corresponding source code, to be distributed under the terms of Sections 1 and 2 above on a medium    customarily used for software interchange; or, &lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;c) Accompany it with the information you received as to the offer to distribute corresponding source code. (This alternative is allowed only for noncommercial distribution and only if you received the program in object code or executable form with such an offer, in accord with Subsection b above.)&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;The source code for a work means the preferred form of the work formaking modifications to it.  For an executable work, complete sourcecode means all the source code for all modules it contains, plus anyassociated interface definition files, plus the scripts used tocontrol compilation and installation of the executable.  However, as aspecial exception, the source code distributed need not includeanything that is normally distributed (in either source or binaryform) with the major components (compiler, kernel, and so on) of theoperating system on which the executable runs, unless that componentitself accompanies the executable.&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;If distribution of executable or object code is made by offeringaccess to copy from a designated place, then offering equivalentaccess to copy the source code from the same place counts asdistribution of the source code, even though third parties are notcompelled to copy the source along with the object code.&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;4. You may not copy, modify, sublicense, or distribute the Programexcept as expressly provided under this License.  Any attemptotherwise to copy, modify, sublicense or distribute the Program isvoid, and will automatically terminate your rights under this License.However, parties who have received copies, or rights, from you underthis License will not have their licenses terminated so long as suchparties remain in full compliance.&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;5. You are not required to accept this License, since you have notsigned it.  However, nothing else grants you permission to modify ordistribute the Program or its derivative works.  These actions areprohibited by law if you do not accept this License.  Therefore, bymodifying or distributing the Program (or any work based on theProgram), you indicate your acceptance of this License to do so, andall its terms and conditions for copying, distributing or modifyingthe Program or works based on it.&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;6. Each time you redistribute the Program (or any work based on theProgram), the recipient automatically receives a license from theoriginal licensor to copy, distribute or modify the Program subject tothese terms and conditions.  You may not impose any furtherrestrictions on the recipients&apos; exercise of the rights granted herein.You are not responsible for enforcing compliance by third parties tothis License.&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;7. If, as a consequence of a court judgment or allegation of patentinfringement or for any other reason (not limited to patent issues),conditions are imposed on you (whether by court order, agreement orotherwise) that contradict the conditions of this License, they do notexcuse you from the conditions of this License.  If you cannotdistribute so as to satisfy simultaneously your obligations under thisLicense and any other pertinent obligations, then as a consequence youmay not distribute the Program at all.  For example, if a patentlicense would not permit royalty-free redistribution of the Program byall those who receive copies directly or indirectly through you, thenthe only way you could satisfy both it and this License would be torefrain entirely from distribution of the Program.&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;If any portion of this section is held invalid or unenforceable underany particular circumstance, the balance of the section is intended toapply and the section as a whole is intended to apply in othercircumstances.&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;It is not the purpose of this section to induce you to infringe anypatents or other property right claims or to contest validity of anysuch claims; this section has the sole purpose of protecting theintegrity of the free software distribution system, which isimplemented by public license practices.  Many people have madegenerous contributions to the wide range of software distributedthrough that system in reliance on consistent application of thatsystem; it is up to the author/donor to decide if he or she is willingto distribute software through any other system and a licensee cannotimpose that choice.&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;This section is intended to make thoroughly clear what is believed tobe a consequence of the rest of this License.&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;8. If the distribution and/or use of the Program is restricted incertain countries either by patents or by copyrighted interfaces, theoriginal copyright holder who places the Program under this Licensemay add an explicit geographical distribution limitation excludingthose countries, so that distribution is permitted only in or amongcountries not thus excluded.  In such case, this License incorporatesthe limitation as if written in the body of this License.&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;9. The Free Software Foundation may publish revised and/or new versionsof the General Public License from time to time.  Such new versions willbe similar in spirit to the present version, but may differ in detail toaddress new problems or concerns.&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Each version is given a distinguishing version number.  If the Programspecifies a version number of this License which applies to it and &quot;anylater version&quot;, you have the option of following the terms and conditionseither of that version or of any later version published by the FreeSoftware Foundation.  If the Program does not specify a version number ofthis License, you may choose any version ever published by the Free SoftwareFoundation.&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;10. If you wish to incorporate parts of the Program into other freeprograms whose distribution conditions are different, write to the authorto ask for permission.  For software which is copyrighted by the FreeSoftware Foundation, write to the Free Software Foundation; we sometimesmake exceptions for this.  Our decision will be guided by the two goalsof preserving the free status of all derivatives of our free software andof promoting the sharing and reuse of software generally.&lt;/p&gt;
&lt;p align=&quot;center&quot; style=&quot; margin-top:18px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:xx-large; font-weight:600;&quot;&gt;&lt;span style=&quot; font-size:xx-large;&quot;&gt;NO WARRANTY&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;11. BECAUSE THE PROGRAM IS LICENSED FREE OF CHARGE, THERE IS NO WARRANTYFOR THE PROGRAM, TO THE EXTENT PERMITTED BY APPLICABLE LAW.  EXCEPT WHENOTHERWISE STATED IN WRITING THE COPYRIGHT HOLDERS AND/OR OTHER PARTIESPROVIDE THE PROGRAM &quot;AS IS&quot; WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSEDOR IMPLIED, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OFMERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.  THE ENTIRE RISK ASTO THE QUALITY AND PERFORMANCE OF THE PROGRAM IS WITH YOU.  SHOULD THEPROGRAM PROVE DEFECTIVE, YOU ASSUME THE COST OF ALL NECESSARY SERVICING,REPAIR OR CORRECTION.&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;12. IN NO EVENT UNLESS REQUIRED BY APPLICABLE LAW OR AGREED TO IN WRITINGWILL ANY COPYRIGHT HOLDER, OR ANY OTHER PARTY WHO MAY MODIFY AND/ORREDISTRIBUTE THE PROGRAM AS PERMITTED ABOVE, BE LIABLE TO YOU FOR DAMAGES,INCLUDING ANY GENERAL, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES ARISINGOUT OF THE USE OR INABILITY TO USE THE PROGRAM (INCLUDING BUT NOT LIMITEDTO LOSS OF DATA OR DATA BEING RENDERED INACCURATE OR LOSSES SUSTAINED BYYOU OR THIRD PARTIES OR A FAILURE OF THE PROGRAM TO OPERATE WITH ANY OTHERPROGRAMS), EVEN IF SUCH HOLDER OR OTHER PARTY HAS BEEN ADVISED OF THEPOSSIBILITY OF SUCH DAMAGES.&lt;/p&gt;
&lt;p align=&quot;center&quot; style=&quot; margin-top:18px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:xx-large; font-weight:600;&quot;&gt;&lt;span style=&quot; font-size:xx-large;&quot;&gt;END OF TERMS AND CONDITIONS&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="aboutbox.cpp" line="32"/>
        <source>Version %1</source>
        <translation>Versio %1</translation>
    </message>
</context>
<context>
    <name>App</name>
    <message>
        <location filename="app.cpp" line="109"/>
        <source>There are unsaved changes.</source>
        <translation>Tietoja tallentamatta.</translation>
    </message>
    <message>
        <location filename="app.cpp" line="112"/>
        <source>Close %1?</source>
        <translation>Sulje %1?</translation>
    </message>
    <message>
        <location filename="app.cpp" line="141"/>
        <location filename="app.cpp" line="143"/>
        <location filename="app.cpp" line="357"/>
        <source>New Venue</source>
        <translation>Uusi Tilaisuus</translation>
    </message>
    <message>
        <location filename="app.cpp" line="142"/>
        <source>Venue Name</source>
        <translation>Tilaisuuden Nimi</translation>
    </message>
    <message>
        <location filename="app.cpp" line="237"/>
        <source>Click new or open to begin</source>
        <translation>Klikkaa &quot;uusi&quot; tai &quot;avaa&quot; aloittaaksesi työskentelyn</translation>
    </message>
    <message>
        <location filename="app.cpp" line="306"/>
        <source>New</source>
        <translation>Uusi</translation>
    </message>
    <message>
        <location filename="app.cpp" line="311"/>
        <source>Open</source>
        <translation>Avaa</translation>
    </message>
    <message>
        <location filename="app.cpp" line="317"/>
        <source>Save</source>
        <translation>Tallenna</translation>
    </message>
    <message>
        <location filename="app.cpp" line="324"/>
        <source>Save As</source>
        <translation>Tallenna nimellä</translation>
    </message>
    <message>
        <location filename="app.cpp" line="330"/>
        <source>Exit</source>
        <translation>Poistu</translation>
    </message>
    <message>
        <location filename="app.cpp" line="340"/>
        <source>File</source>
        <translation>Tiedosto</translation>
    </message>
    <message>
        <location filename="app.cpp" line="358"/>
        <source>Do you wish to discard the current venue (%1)?</source>
        <translation>Haluatko hukata nykyisen tilaisuuden (%1) tiedot?</translation>
    </message>
    <message>
        <location filename="app.cpp" line="372"/>
        <source>Open Venue</source>
        <translation>Avaa tilaisuus</translation>
    </message>
    <message>
        <location filename="app.cpp" line="384"/>
        <source>Unable to load venue</source>
        <translation>Tilaisuutta ei voitu avata</translation>
    </message>
    <message>
        <location filename="app.cpp" line="385"/>
        <source>File may be corrupt or you don&apos;t have permission to read it</source>
        <translation>Tiedosto voi olla rikki tai sinulla ei ole oikeutta lukea tiedostoa</translation>
    </message>
    <message>
        <location filename="app.cpp" line="402"/>
        <location filename="app.cpp" line="424"/>
        <source>Save failed</source>
        <translation>Tallennus epäonnistui</translation>
    </message>
    <message>
        <location filename="app.cpp" line="416"/>
        <source>Save Venue As...</source>
        <translation>Tallenna tilaisuus nimellä...</translation>
    </message>
    <message>
        <location filename="app.cpp" line="441"/>
        <source>Edit Venue</source>
        <translation>Muokkaa tilaisuutta</translation>
    </message>
    <message>
        <location filename="app.cpp" line="446"/>
        <source>Rename Venue</source>
        <translation>Nimeä tilaisuus</translation>
    </message>
    <message>
        <location filename="app.cpp" line="451"/>
        <source>Venue Monitor</source>
        <translation>Tilaisuuden seuranta</translation>
    </message>
    <message>
        <location filename="app.cpp" line="456"/>
        <source>Current Time</source>
        <translation>Kellonaika</translation>
    </message>
    <message>
        <location filename="app.cpp" line="461"/>
        <source>Run</source>
        <translation>Käynnistä</translation>
    </message>
    <message>
        <location filename="app.cpp" line="466"/>
        <source>Pause</source>
        <translation>Keskeytä</translation>
    </message>
    <message>
        <location filename="app.cpp" line="471"/>
        <location filename="app.cpp" line="613"/>
        <source>Stop</source>
        <translation>Pysäytä</translation>
    </message>
    <message>
        <location filename="app.cpp" line="476"/>
        <source>Previous cue</source>
        <translation>Edellinen aloitusmerkki</translation>
    </message>
    <message>
        <location filename="app.cpp" line="481"/>
        <source>Next cue</source>
        <translation>Seuraava aloitusmerkki</translation>
    </message>
    <message>
        <location filename="app.cpp" line="489"/>
        <source>Venue</source>
        <translation>Tilaisuus</translation>
    </message>
    <message>
        <location filename="app.cpp" line="584"/>
        <source>Change Venue Name</source>
        <translation>Muokkaa tilaisuuden nimeä</translation>
    </message>
    <message>
        <location filename="app.cpp" line="585"/>
        <source>Venue name</source>
        <translation>Tilaisuuden nimi</translation>
    </message>
    <message>
        <location filename="app.cpp" line="614"/>
        <source>Do you really want to STOP the venue?</source>
        <translation>Haluatko todella PYSÄYTTÄÄ tilaisuuden?</translation>
    </message>
    <message>
        <location filename="app.cpp" line="639"/>
        <source>Show Stage View</source>
        <translation>Näytä lavanäkymä</translation>
    </message>
    <message>
        <location filename="app.cpp" line="644"/>
        <source>Hide Stage View</source>
        <translation>Piilota lavanäkymä</translation>
    </message>
    <message>
        <location filename="app.cpp" line="649"/>
        <location filename="app.cpp" line="733"/>
        <source>Stage Monitor</source>
        <translation>Lavan seuranta</translation>
    </message>
    <message>
        <location filename="app.cpp" line="662"/>
        <source>Stage</source>
        <translation>Lava</translation>
    </message>
    <message>
        <location filename="app.cpp" line="691"/>
        <source>Stage View</source>
        <translation>Lavanäkymä</translation>
    </message>
    <message>
        <location filename="app.cpp" line="776"/>
        <source>Window</source>
        <translation>Ikkuna</translation>
    </message>
    <message>
        <location filename="app.cpp" line="813"/>
        <source>Cascade</source>
        <translation>Lomittain</translation>
    </message>
    <message>
        <location filename="app.cpp" line="815"/>
        <source>Tile</source>
        <translation>Vierekkäin</translation>
    </message>
    <message>
        <location filename="app.cpp" line="856"/>
        <location filename="app.cpp" line="877"/>
        <source>Help</source>
        <translation>Apua</translation>
    </message>
    <message>
        <location filename="app.cpp" line="863"/>
        <source>About %1</source>
        <translation>Tietoja %1ista</translation>
    </message>
    <message>
        <location filename="app.cpp" line="869"/>
        <source>About QT</source>
        <translation>Tietoja QT:sta</translation>
    </message>
    <message>
        <location filename="app.cpp" line="107"/>
        <source>Are you sure you wish to close %1?</source>
        <translation>Haluatko varmasti sulkea %1in?</translation>
    </message>
    <message>
        <location filename="app.cpp" line="654"/>
        <source>Configure Stage View</source>
        <translation>Muokkaa lavanäkymää</translation>
    </message>
</context>
<context>
    <name>CueEditor</name>
    <message>
        <location filename="cueeditor.cpp" line="46"/>
        <source>%1 - Edit Cue</source>
        <translation>%1 - Muokkaa aloitusmerkkiä</translation>
    </message>
    <message>
        <location filename="cueeditor.cpp" line="58"/>
        <source>Cue</source>
        <translation>Aloitusmerkki</translation>
    </message>
    <message>
        <location filename="cueeditor.cpp" line="73"/>
        <source>Duration</source>
        <translation>Kesto</translation>
    </message>
</context>
<context>
    <name>EventEditor</name>
    <message>
        <location filename="eventeditor.cpp" line="63"/>
        <source>Event Editor</source>
        <translation>Tapahtumaeditori</translation>
    </message>
    <message>
        <location filename="eventeditor.cpp" line="111"/>
        <source>Add cue</source>
        <translation>Lisää aloitusmerkki</translation>
    </message>
    <message>
        <location filename="eventeditor.cpp" line="115"/>
        <location filename="eventeditor.cpp" line="250"/>
        <source>Remove cue</source>
        <translation>Poista aloitusmerkki</translation>
    </message>
    <message>
        <location filename="eventeditor.cpp" line="119"/>
        <source>Edit cue</source>
        <translation>Muokkaa aloitusmerkkiä</translation>
    </message>
    <message>
        <location filename="eventeditor.cpp" line="123"/>
        <source>Raise cue</source>
        <translation>Nosta aloitusmerkkiä</translation>
    </message>
    <message>
        <location filename="eventeditor.cpp" line="128"/>
        <source>Lower cue</source>
        <translation>Laske aloitusmerkkiä</translation>
    </message>
    <message>
        <location filename="eventeditor.cpp" line="147"/>
        <source>Name</source>
        <translation>Nimi</translation>
    </message>
    <message>
        <location filename="eventeditor.cpp" line="179"/>
        <source>Order</source>
        <translation>Järjestys</translation>
    </message>
    <message>
        <location filename="eventeditor.cpp" line="179"/>
        <source>Cue</source>
        <translation>Aloitusmerkki</translation>
    </message>
    <message>
        <location filename="eventeditor.cpp" line="179"/>
        <source>Duration</source>
        <translation>Kesto</translation>
    </message>
    <message>
        <location filename="eventeditor.cpp" line="251"/>
        <source>Remove cue %1 from event %2 position %3?</source>
        <translation>Poista aloitusmerkki %1 tapahtuman %2 kohdasta %3?</translation>
    </message>
    <message>
        <location filename="eventeditor.cpp" line="334"/>
        <source>Unable to rename event</source>
        <translation>Tapahtumaa ei voida uudelleennimetä</translation>
    </message>
    <message>
        <location filename="eventeditor.cpp" line="335"/>
        <source>Another event by the name %1 already exists!</source>
        <translation>Tilaisuudessa on jo toinen tapahtuma nimeltä %1!</translation>
    </message>
    <message>
        <location filename="eventeditor.cpp" line="336"/>
        <source>You must give this event some other name</source>
        <translation>Anna jokin toinen nimi tälle tapahtumalle</translation>
    </message>
</context>
<context>
    <name>HelpBrowser</name>
    <message>
        <location filename="helpbrowser.cpp" line="45"/>
        <source>Previous page</source>
        <translation>Edellinen sivu</translation>
    </message>
    <message>
        <location filename="helpbrowser.cpp" line="49"/>
        <source>Next page</source>
        <translation>Seuraava sivu</translation>
    </message>
    <message>
        <location filename="helpbrowser.cpp" line="55"/>
        <source>%1 - Document Browser</source>
        <translation>%1 - Käyttöohjeiden selain</translation>
    </message>
    <message>
        <location filename="helpbrowser.cpp" line="61"/>
        <source>Document Browser</source>
        <translation>Käyttöohjeiden selain</translation>
    </message>
</context>
<context>
    <name>StageView</name>
    <message>
        <location filename="stageview.cpp" line="116"/>
        <location filename="stageview.cpp" line="148"/>
        <source>Stopped</source>
        <translation>Pysäytetty</translation>
    </message>
    <message>
        <location filename="stageview.cpp" line="142"/>
        <source>Finished</source>
        <translation>Päättynyt</translation>
    </message>
    <message>
        <location filename="stageview.cpp" line="157"/>
        <source>Next: %1 (%2h %3m)</source>
        <translation>Seuraavaksi: %1 (%2h %3m)</translation>
    </message>
</context>
<context>
    <name>StageViewEditor</name>
    <message>
        <location filename="stagevieweditor.ui" line="13"/>
        <source>Stage View Editor</source>
        <translation>Lavanäyttö</translation>
    </message>
    <message>
        <location filename="stagevieweditor.ui" line="23"/>
        <source>Background</source>
        <translation>Tausta</translation>
    </message>
    <message>
        <location filename="stagevieweditor.ui" line="29"/>
        <location filename="stagevieweditor.ui" line="111"/>
        <location filename="stagevieweditor.ui" line="193"/>
        <location filename="stagevieweditor.ui" line="272"/>
        <location filename="stagevieweditor.ui" line="453"/>
        <source>Color</source>
        <translation>Väri</translation>
    </message>
    <message>
        <location filename="stagevieweditor.ui" line="36"/>
        <source>Stage view background color</source>
        <translation>Lavanäytön taustan väri</translation>
    </message>
    <message>
        <location filename="stagevieweditor.ui" line="46"/>
        <source>Change the background color for stage view</source>
        <translation>Vaihda lavanäytön taustan väriä</translation>
    </message>
    <message>
        <location filename="stagevieweditor.ui" line="74"/>
        <source>Event</source>
        <translation>Tapahtuma</translation>
    </message>
    <message>
        <location filename="stagevieweditor.ui" line="80"/>
        <location filename="stagevieweditor.ui" line="162"/>
        <location filename="stagevieweditor.ui" line="244"/>
        <location filename="stagevieweditor.ui" line="425"/>
        <source>Font</source>
        <translation>Kirjasin</translation>
    </message>
    <message>
        <location filename="stagevieweditor.ui" line="87"/>
        <source>The font used for the current event</source>
        <translation>Nykyisen tapahtuman kirjasin</translation>
    </message>
    <message>
        <location filename="stagevieweditor.ui" line="97"/>
        <source>Change the font used for the current event</source>
        <translation>Muokkaa nykyisen tapahtuman kirjasinta</translation>
    </message>
    <message>
        <location filename="stagevieweditor.ui" line="118"/>
        <source>The color used for the current event</source>
        <translation>Nykyisen tapahtuman väri</translation>
    </message>
    <message>
        <location filename="stagevieweditor.ui" line="128"/>
        <source>Change the color for the current event</source>
        <translation>Muokkaa nykyisen tapahtuman väriä</translation>
    </message>
    <message>
        <location filename="stagevieweditor.ui" line="156"/>
        <source>Current cue</source>
        <translation>Nykyinen aloitusmerkki</translation>
    </message>
    <message>
        <location filename="stagevieweditor.ui" line="169"/>
        <source>The font used for the current cue</source>
        <translation>Nykyisen aloitusmerkin kirjasin</translation>
    </message>
    <message>
        <location filename="stagevieweditor.ui" line="179"/>
        <source>Change the font used for the current cue</source>
        <translation>Muokkaa nykyisen aloitusmerkin kirjasinta</translation>
    </message>
    <message>
        <location filename="stagevieweditor.ui" line="200"/>
        <source>The color used for the current cue</source>
        <translation>Nykyisen aloitusmerkin väri</translation>
    </message>
    <message>
        <location filename="stagevieweditor.ui" line="210"/>
        <source>Change the color for the current cue</source>
        <translation>Muokkaa nykyisen aloitusmerkin väriä</translation>
    </message>
    <message>
        <location filename="stagevieweditor.ui" line="238"/>
        <source>Remaining time</source>
        <translation>Jäljelläoleva aika</translation>
    </message>
    <message>
        <location filename="stagevieweditor.ui" line="251"/>
        <source>The font used for the remaining time</source>
        <translation>Jäljelläolevan ajan kirjasin</translation>
    </message>
    <message>
        <location filename="stagevieweditor.ui" line="278"/>
        <source>Warning</source>
        <translation>Varoitus</translation>
    </message>
    <message>
        <location filename="stagevieweditor.ui" line="285"/>
        <source>Amount of time left until remaining time status changes to warning</source>
        <translation>Jäljelläoleva aikaraja, jonka alittuessa tila vaihtuu varoitukseksi</translation>
    </message>
    <message>
        <location filename="stagevieweditor.ui" line="288"/>
        <location filename="stagevieweditor.ui" line="336"/>
        <source>hh:mm:ss</source>
        <translation>tt:mm:ss</translation>
    </message>
    <message>
        <location filename="stagevieweditor.ui" line="302"/>
        <source>The color used when remaining time is less than the warning threshold</source>
        <translation>Tekstin väri, kun jäljelläoleva aika alittaa varoitusrajan</translation>
    </message>
    <message>
        <location filename="stagevieweditor.ui" line="312"/>
        <source>Change the color for remaining time warning status</source>
        <translation>Muokkaa jäljelläolevan ajan varoitustilan tekstin väriä</translation>
    </message>
    <message>
        <location filename="stagevieweditor.ui" line="326"/>
        <source>Critical</source>
        <translation>Kriittinen</translation>
    </message>
    <message>
        <location filename="stagevieweditor.ui" line="333"/>
        <source>Amount of time left until the remaining time status changes to critical</source>
        <translation>Jäljelläoleva aikaraja, jonka alittuessa tila vaihtuu kriittiseksi</translation>
    </message>
    <message>
        <location filename="stagevieweditor.ui" line="350"/>
        <source>The color used when the remaining time is less than the critical threshold</source>
        <translation>Tekstin väri, kun jäljelläoleva aika alittaa kriittisen rajan</translation>
    </message>
    <message>
        <location filename="stagevieweditor.ui" line="360"/>
        <source>Change the color for remaining time critical status</source>
        <translation>Muokkaa jäljelläolevan ajan kriittisen tilan tekstin väriä</translation>
    </message>
    <message>
        <location filename="stagevieweditor.ui" line="374"/>
        <source>Flash remaining time when it is less than the critical threshold</source>
        <translation>Välkytä jäljelläolevaa aikatekstiä, kun aika alittaa kriittisen rajan</translation>
    </message>
    <message>
        <location filename="stagevieweditor.ui" line="377"/>
        <source>Flash</source>
        <translation>Välkytys</translation>
    </message>
    <message>
        <location filename="stagevieweditor.ui" line="387"/>
        <source>The color used for the remaining time</source>
        <translation>Jäljelläolevan ajan väri</translation>
    </message>
    <message>
        <location filename="stagevieweditor.ui" line="408"/>
        <source>Normal</source>
        <translation>Normaali</translation>
    </message>
    <message>
        <location filename="stagevieweditor.ui" line="419"/>
        <source>Next cue</source>
        <translation>Seuraava aloitusmerkki</translation>
    </message>
    <message>
        <location filename="stagevieweditor.ui" line="432"/>
        <source>The font used for the next cue</source>
        <translation>Seuraavan aloitusmerkin kirjasin</translation>
    </message>
    <message>
        <location filename="stagevieweditor.ui" line="460"/>
        <source>The color used for the next cue</source>
        <translation>Seuraavan aloitusmerkin väri</translation>
    </message>
</context>
<context>
    <name>TimeView</name>
    <message>
        <location filename="timeview.cpp" line="40"/>
        <source>Current time</source>
        <translation>Kellonaika</translation>
    </message>
    <message>
        <location filename="timeview.cpp" line="168"/>
        <source>Font</source>
        <translation>Kirjasin</translation>
    </message>
    <message>
        <location filename="timeview.cpp" line="170"/>
        <source>Font Color</source>
        <translation>Kirjasimen väri</translation>
    </message>
    <message>
        <location filename="timeview.cpp" line="173"/>
        <source>Background Color</source>
        <translation>Taustan väri</translation>
    </message>
</context>
<context>
    <name>Venue</name>
    <message>
        <location filename="venue.cpp" line="488"/>
        <source>%1: %2. Line %3, column %4.</source>
        <translation>%1: %2. Rivi %3, sarake %4.</translation>
    </message>
    <message>
        <location filename="venue.cpp" line="489"/>
        <source>File load error</source>
        <translation>Tiedoston latausvirhe</translation>
    </message>
    <message>
        <location filename="venue.cpp" line="496"/>
        <source>File open error</source>
        <translation>Tiedoston avausvirhe</translation>
    </message>
    <message>
        <location filename="venue.cpp" line="497"/>
        <source>Unable to open %1: %2</source>
        <translation>Tiedostoa %1 ei voida avata: %2</translation>
    </message>
</context>
<context>
    <name>VenueEditor</name>
    <message>
        <location filename="venueeditor.cpp" line="53"/>
        <source>Venue Editor</source>
        <translation>Tilaisuus-editori</translation>
    </message>
    <message>
        <location filename="venueeditor.cpp" line="103"/>
        <source>Add Event</source>
        <translation>Lisää tapahtuma</translation>
    </message>
    <message>
        <location filename="venueeditor.cpp" line="108"/>
        <source>Remove Event</source>
        <translation>Poista tapahtuma</translation>
    </message>
    <message>
        <location filename="venueeditor.cpp" line="113"/>
        <source>Edit Event</source>
        <translation>Muokkaa tapahtumaa</translation>
    </message>
    <message>
        <location filename="venueeditor.cpp" line="116"/>
        <source>Raise Event</source>
        <translation>Nosta tapahtumaa</translation>
    </message>
    <message>
        <location filename="venueeditor.cpp" line="121"/>
        <source>Lower Event</source>
        <translation>Laske tapahtumaa</translation>
    </message>
    <message>
        <location filename="venueeditor.cpp" line="133"/>
        <source>Events</source>
        <translation>Tapahtumat</translation>
    </message>
    <message>
        <location filename="venueeditor.cpp" line="150"/>
        <source>New Event</source>
        <translation>Uusi tapahtuma</translation>
    </message>
    <message>
        <location filename="venueeditor.cpp" line="151"/>
        <source>Event name</source>
        <translation>Tapahtuman nimi</translation>
    </message>
    <message>
        <location filename="venueeditor.cpp" line="153"/>
        <source>New event</source>
        <translation>Uusi tapahtuma</translation>
    </message>
    <message>
        <location filename="venueeditor.cpp" line="166"/>
        <source>Unable to add event</source>
        <translation>Tapahtumaa ei voitu lisätä</translation>
    </message>
    <message>
        <location filename="venueeditor.cpp" line="167"/>
        <source>Another event by the name &quot;%1&quot;already exists</source>
        <translation>Tilaisuudessa on jo toinen tapahtuma nimeltä &quot;%1&quot;</translation>
    </message>
    <message>
        <location filename="venueeditor.cpp" line="184"/>
        <source>Remove Event?</source>
        <translation>Poista tapahtuma?</translation>
    </message>
    <message>
        <location filename="venueeditor.cpp" line="185"/>
        <source>Remove event &quot;%1&quot; from venue &quot;%2&quot;?</source>
        <translation>Poista tapahtuma &quot;%1&quot; tilaisuudesta &quot;%2&quot;?</translation>
    </message>
    <message>
        <location filename="venueeditor.cpp" line="269"/>
        <source>Order</source>
        <translation>Järjestys</translation>
    </message>
    <message>
        <location filename="venueeditor.cpp" line="269"/>
        <source>Event</source>
        <translation>Tapahtuma</translation>
    </message>
    <message>
        <location filename="venueeditor.cpp" line="270"/>
        <source>Duration</source>
        <translation>Kesto</translation>
    </message>
</context>
<context>
    <name>VenueMonitor</name>
    <message>
        <location filename="venuemonitor.cpp" line="91"/>
        <source>Close</source>
        <translation>Sulje</translation>
    </message>
    <message>
        <location filename="venuemonitor.cpp" line="103"/>
        <source>Cue</source>
        <translation>Aloitusmerkki</translation>
    </message>
    <message>
        <location filename="venuemonitor.cpp" line="103"/>
        <source>Time left</source>
        <translation>Aikaa jäljellä</translation>
    </message>
    <message>
        <location filename="venuemonitor.cpp" line="103"/>
        <source>Waiting time</source>
        <translation>Odotusaika</translation>
    </message>
</context>
</TS>
