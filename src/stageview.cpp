/*
  StageManager
  stageview.cpp

  Copyright (C) Heikki Junnila

  This program is free software; you can redistribute it and/or
  modify it under the terms of the GNU General Public License
  Version 2 as published by the Free Software Foundation.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details. The license is
  in the file "COPYING".

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*/

#include <QApplication>
#include <QPaintEvent>
#include <QVBoxLayout>
#include <QPainter>
#include <QPalette>
#include <QAction>
#include <QLabel>
#include <QColor>
#include <QBrush>
#include <QFont>
#include <QRect>
#include <QtXml>

#include "stageview.h"
#include "venue.h"
#include "cue.h"
#include "app.h"

#include "stageviewproperties.h"

/*****************************************************************************
 * StageView
 *****************************************************************************/

StageView::StageView(QWidget* parent, Venue* venue, Qt::WindowFlags flags)
    : QWidget(parent, flags)
{
    m_status = StageView::Normal;

    initView();
    setVenue(venue);
    slotMasterTick();
}

StageView::~StageView()
{
}

/*****************************************************************************
 * View
 *****************************************************************************/

void StageView::initView()
{
    setAutoFillBackground(true);

    new QVBoxLayout(this);
    layout()->setSpacing(0);
    layout()->setContentsMargins(0, 0, 0, 0);

    /* Cue time left label */
    m_cueLeft = new QLabel(this);
    layout()->addWidget(m_cueLeft);
    m_cueLeft->setAlignment(Qt::AlignHCenter | Qt::AlignVCenter);

    /* Current cue label */
    m_cue = new QLabel(this);
    layout()->addWidget(m_cue);
    m_cue->setAlignment(Qt::AlignHCenter | Qt::AlignVCenter);
    m_cue->setWordWrap(true);

    /* Next cue label */
    m_nextCue = new QLabel(this);
    layout()->addWidget(m_nextCue);
    m_nextCue->setAlignment(Qt::AlignHCenter | Qt::AlignVCenter);
    m_nextCue->setWordWrap(true);
}

void StageView::showMultiHead()
{
    /* Create an action for esc key to close the fullscreen view
       as in open office impress */
    QAction* action = new QAction(this);
    action->setShortcut(QKeySequence("Esc"));
    connect(action, SIGNAL(triggered(bool)), this, SLOT(close()));
    addAction(action);

    QWidget::show();
}

void StageView::slotMasterTick()
{
    Cue* cue;

    /* Current cue & time left */
    cue = m_venue->currentCue();
    if (cue != NULL)
    {
        int secs = cue->timeLeft();
        if (secs >= 0)
        {
            m_cueLeft->setText(
                App::secsToTime(secs).toString(Qt::ISODate));
            m_cue->setText(cue->name());

            /* Change color & flashing according to thresholds */
            if (secs <= m_properties.criticalTime)
                setStatus(StageView::Critical);
            else if (secs <= m_properties.warningTime)
                setStatus(StageView::Warning);
            else
                setStatus(StageView::Normal);
        }
        else
        {
            secs = 0;
            m_cueLeft->setText(
                App::secsToTime(secs).toString(Qt::ISODate));
            m_cue->setText(tr("Finished"));
        }
    }
    else
    {
        m_cueLeft->setText("00:00:00");
        m_cue->setText(tr("Stopped"));
        setStatus(Normal);
    }

    /* Next cue & time left */
    cue = m_venue->nextCue();
    if (cue != NULL)
    {
        QTime time = QTime().addSecs(cue->duration());
        m_nextCue->setText(tr("Next: %1 (%2h %3m)") .arg(cue->name())
                    .arg(time.hour()).arg(time.minute()));
    }
    else
    {
        m_nextCue->setText("");
    }
}

/*****************************************************************************
 * Status
 *****************************************************************************/

void StageView::setStatus(StageView::Status status)
{
    QPalette pal;

    if (m_status == status)
        return;
    m_status = status;

    switch (status)
    {
    default:
    case StageView::Normal:
        pal = m_cueLeft->palette();
        pal.setColor(QPalette::WindowText,
                 QColor(m_properties.remainingTimeColorNormal));
        m_cueLeft->setPalette(pal);
        m_cueLeft->setAutoFillBackground(false);
        break;

    case StageView::Warning:
        pal = m_cueLeft->palette();
        pal.setColor(QPalette::WindowText,
                 QColor(m_properties.remainingTimeColorWarning));
        m_cueLeft->setPalette(pal);
        m_cueLeft->setAutoFillBackground(false);
        break;

    case StageView::Critical:
        pal = m_cueLeft->palette();
        pal.setColor(QPalette::WindowText,
                 QColor(m_properties.remainingTimeColorCritical));
        m_cueLeft->setPalette(pal);
        m_cueLeft->setAutoFillBackground(true);

        QTimer::singleShot(500, this, SLOT(slotFlashTimeout()));

        break;
    }
}

void StageView::slotFlashTimeout()
{
    static bool invert = false;

    if (m_status == Critical)
    {
        QPalette pal(m_cueLeft->palette());
        QColor fg(m_properties.remainingTimeColorCritical);
        QColor bg(m_properties.bgColor);

        if (invert == true)
        {
            pal.setColor(QPalette::WindowText, bg);
            pal.setColor(QPalette::Window, fg);
            invert = false;
        }
        else
        {
            pal.setColor(QPalette::WindowText, fg);
            pal.setColor(QPalette::Window, bg);
            invert = true;
        }

        m_cueLeft->setPalette(pal);

        QTimer::singleShot(500, this, SLOT(slotFlashTimeout()));
    }
}

/*****************************************************************************
 * Venue
 *****************************************************************************/

void StageView::setVenue(Venue* venue)
{
    Q_ASSERT(venue != NULL);
    m_venue = venue;
}

/*****************************************************************************
 * Properties
 *****************************************************************************/

void StageView::setProperties(const StageViewProperties& properties)
{
    QPalette pal;
    QFont font;

    m_properties = properties;

    /* Background */
    pal = palette();
    pal.setColor(QPalette::Window, QColor(properties.bgColor));
    setPalette(pal);

    /* Remaining time */
    pal = m_cueLeft->palette();
    if (m_status == StageView::Normal)
    {
        pal.setColor(QPalette::WindowText,
                 QColor(properties.remainingTimeColorNormal));
    }
    else if (m_status == StageView::Warning)
    {
        pal.setColor(QPalette::WindowText,
                 QColor(properties.remainingTimeColorWarning));
    }
    else
    {
        pal.setColor(QPalette::WindowText,
                 QColor(properties.remainingTimeColorCritical));
        pal.setColor(QPalette::Window,
                 QColor(properties.bgColor));
    }
    m_cueLeft->setPalette(pal);
    font.fromString(properties.remainingTimeFont);
    m_cueLeft->setFont(font);

    /* Current cue */
    pal = m_cue->palette();
    pal.setColor(QPalette::WindowText, QColor(properties.cueColor));
    m_cue->setPalette(pal);
    font.fromString(properties.cueFont);
    m_cue->setFont(font);

    /* Next cue */
    pal = m_nextCue->palette();
    pal.setColor(QPalette::WindowText, QColor(properties.nextCueColor));
    m_nextCue->setPalette(pal);
    font.fromString(properties.nextCueFont);
    m_nextCue->setFont(font);
}

const StageViewProperties StageView::properties() const
{
    return m_properties;
}

