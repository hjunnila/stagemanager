/*
  StageManager
  stageview.h

  Copyright (C) Heikki Junnila

  This program is free software; you can redistribute it and/or
  modify it under the terms of the GNU General Public License
  Version 2 as published by the Free Software Foundation.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details. The license is
  in the file "COPYING".

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*/

#ifndef STAGEVIEW_H
#define STAGEVIEW_H

#include <QCloseEvent>
#include <QWidget>
#include <QBrush>
#include <QFont>
#include <QPen>

#include "stageviewproperties.h"

class QDomDocument;
class QDomElement;
class QLabel;

class StageView;
class Venue;

class StageView : public QWidget
{
    Q_OBJECT

public:
    StageView(QWidget* parent, Venue* venue, Qt::WindowFlags flags = 0);
    ~StageView();

private:
    Q_DISABLE_COPY(StageView)

    /*********************************************************************
     * View
     *********************************************************************/
protected:
    void initView();

public slots:
    void showMultiHead();
    void slotMasterTick();

protected:
    QLabel* m_event;
    QLabel* m_cueLeft;
    QLabel* m_cue;
    QLabel* m_nextCue;

    /*********************************************************************
     * Status
     *********************************************************************/
public:
    enum Status { Normal, Warning, Critical };

    void setStatus(Status status);
    Status status() const { return m_status; }

protected slots:
    void slotFlashTimeout();

protected:
    Status m_status;

    /*********************************************************************
     * Venue
     *********************************************************************/
public:
    void setVenue(Venue* venue);

protected:
    Venue* m_venue;

    /*********************************************************************
     * Properties
     *********************************************************************/
public:
    void setProperties(const StageViewProperties& properties);
    const StageViewProperties properties() const;

protected:
    StageViewProperties m_properties;
};

#endif
