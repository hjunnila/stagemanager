/*
  StageManager
  stagevieweditor.cpp

  Copyright (C) Heikki Junnila

  This program is free software; you can redistribute it and/or
  modify it under the terms of the GNU General Public License
  Version 2 as published by the Free Software Foundation.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details. The license is
  in the file "COPYING".

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*/

#include <QColorDialog>
#include <QRadioButton>
#include <QPushButton>
#include <QFileDialog>
#include <QFontDialog>
#include <QPaintEvent>
#include <QLineEdit>
#include <QPicture>
#include <QPainter>
#include <QPalette>
#include <QColor>
#include <QBrush>
#include <QLabel>

#include "stagevieweditor.h"
#include "app.h"

StageViewEditor::StageViewEditor(QWidget* parent,
         const StageViewProperties& properties) : QDialog(parent)
{
    setupUi(this);
    initView();

    setProperties(properties);
}

StageViewEditor::~StageViewEditor()
{
}

/*****************************************************************************
 * Configuration
 *****************************************************************************/

void StageViewEditor::setProperties(const StageViewProperties& properties)
{
    QPalette pal;
    QFont font;
    QString str;

    m_properties = properties;

    /* BG Color */
    pal = m_bgColorEdit->palette();
    pal.setColor(QPalette::Base, QColor(properties.bgColor));
    m_bgColorEdit->setPalette(pal);
    m_bgColorEdit->setAutoFillBackground(true);

    /* Cue Font */
    font.fromString(properties.cueFont);
    m_cueFontEdit->setText(QString("%1 - %2").arg(font.family())
                         .arg(font.pointSize()));
    font.setPointSize(9);
    m_cueFontEdit->setFont(font);

    /* Cue Color */
    pal = m_cueColorEdit->palette();
    pal.setColor(QPalette::Base, QColor(properties.cueColor));
    m_cueColorEdit->setPalette(pal);
    m_cueColorEdit->setAutoFillBackground(true);

    /* Remaining Time Font */
    font.fromString(properties.remainingTimeFont);
    m_remainingTimeFontEdit->setText(QString("%1 - %2").arg(font.family())
                            .arg(font.pointSize()));
    font.setPointSize(9);
    m_remainingTimeFontEdit->setFont(font);

    /* Cue Remaining Time Color - Normal */
    pal = m_remainingTimeColorNormalEdit->palette();
    pal.setColor(QPalette::Base, properties.remainingTimeColorNormal);
    m_remainingTimeColorNormalEdit->setPalette(pal);
    m_remainingTimeColorNormalEdit->setAutoFillBackground(true);

    /* Cue Remaining Time Color - Warning */
    pal = m_remainingTimeColorWarningEdit->palette();
    pal.setColor(QPalette::Base, properties.remainingTimeColorWarning);
    m_remainingTimeColorWarningEdit->setPalette(pal);
    m_remainingTimeColorWarningEdit->setAutoFillBackground(true);
    m_remainingTimeColorWarningSpin->setTime(
                App::secsToTime(properties.warningTime));

    /* Cue Remaining Time Color - Critical */
    pal = m_remainingTimeColorCriticalEdit->palette();
    pal.setColor(QPalette::Base, properties.remainingTimeColorCritical);
    m_remainingTimeColorCriticalEdit->setPalette(pal);
    m_remainingTimeColorCriticalEdit->setAutoFillBackground(true);
    m_remainingTimeColorCriticalSpin->setTime(
                App::secsToTime(properties.criticalTime));

    /* Critical flash */
    m_remainingTimeCriticalFlashButton->setChecked(
                        properties.criticalFlash);

    /* Next Cue Font */
    font.fromString(properties.nextCueFont);
    m_nextCueFontEdit->setText(QString("%1 - %2").arg(font.family())
                             .arg(font.pointSize()));
    font.setPointSize(9);
    m_nextCueFontEdit->setFont(font);

    /* Next Cue Color */
    pal = m_cueColorEdit->palette();
    pal.setColor(QPalette::Base, QColor(properties.nextCueColor));
    m_nextCueColorEdit->setPalette(pal);
    m_nextCueColorEdit->setAutoFillBackground(true);
}

const StageViewProperties StageViewEditor::properties() const
{
    return m_properties;
}

void StageViewEditor::initView()
{
    /* Background */
    connect(m_bgColorButton, SIGNAL(clicked()),
        this, SLOT(slotBGColorButtonClicked()));

    /* Current Cue */
    connect(m_cueColorButton, SIGNAL(clicked()),
        this, SLOT(slotCueColorButtonClicked()));
    connect(m_cueFontButton, SIGNAL(clicked()),
        this, SLOT(slotCueFontButtonClicked()));

    /* Remaining Time */
    connect(m_remainingTimeFontButton, SIGNAL(clicked()),
        this, SLOT(slotRemainingTimeFontButtonClicked()));

    connect(m_remainingTimeColorNormalButton, SIGNAL(clicked()),
        this, SLOT(slotRemainingTimeColorNormalButtonClicked()));
    connect(m_remainingTimeColorWarningButton, SIGNAL(clicked()),
        this, SLOT(slotRemainingTimeColorWarningButtonClicked()));
    connect(m_remainingTimeColorCriticalButton, SIGNAL(clicked()),
        this, SLOT(slotRemainingTimeColorCriticalButtonClicked()));

    connect(m_remainingTimeColorWarningSpin,
        SIGNAL(timeChanged(const QTime&)),
        this,
        SLOT(slotRemainingTimeColorWarningTimeChanged(const QTime&)));

    connect(m_remainingTimeColorCriticalSpin,
        SIGNAL(timeChanged(const QTime&)),
        this,
        SLOT(slotRemainingTimeColorCriticalTimeChanged(const QTime&)));

    connect(m_remainingTimeCriticalFlashButton, SIGNAL(clicked()),
        this, SLOT(slotRemainingTimeCriticalFlashButtonClicked()));

    /* Next Cue */
    connect(m_nextCueFontButton, SIGNAL(clicked()),
        this, SLOT(slotNextCueFontButtonClicked()));
    connect(m_nextCueColorButton, SIGNAL(clicked()),
        this, SLOT(slotNextCueColorButtonClicked()));
}

/*****************************************************************************
 * Background page
 *****************************************************************************/

void StageViewEditor::slotBGColorButtonClicked()
{
    QColor color(m_properties.bgColor);

    color = QColorDialog::getColor(color, this);
    if (color.isValid() == true)
        m_properties.bgColor = color.rgb();
    setProperties(m_properties);
}

/*****************************************************************************
 * Current cue page
 *****************************************************************************/

void StageViewEditor::slotCueFontButtonClicked()
{
    bool ok = false;
    QFont font;

    font.fromString(m_properties.cueFont);
    font = QFontDialog::getFont(&ok, font, this);
    if (ok == true)
        m_properties.cueFont = font.toString();
    setProperties(m_properties);
}

void StageViewEditor::slotCueColorButtonClicked()
{
    QColor color(m_properties.cueColor);

    color = QColorDialog::getColor(color, this);
    if (color.isValid() == true)
        m_properties.cueColor = color.rgb();
    setProperties(m_properties);
}

/*****************************************************************************
 * Remaining time page
 *****************************************************************************/

void StageViewEditor::slotRemainingTimeFontButtonClicked()
{
    bool ok = false;
    QFont font;

    font.fromString(m_properties.remainingTimeFont);
    font = QFontDialog::getFont(&ok, font, this);
    if (ok == true)
        m_properties.remainingTimeFont = font.toString();
    setProperties(m_properties);
}

void StageViewEditor::slotRemainingTimeColorNormalButtonClicked()
{
    QColor color(m_properties.remainingTimeColorNormal);

    color = QColorDialog::getColor(color, this);
    if (color.isValid() == true)
        m_properties.remainingTimeColorNormal = color.rgb();
    setProperties(m_properties);
}

void StageViewEditor::slotRemainingTimeColorWarningButtonClicked()
{
    QColor color(m_properties.remainingTimeColorWarning);

    color = QColorDialog::getColor(color, this);
    if (color.isValid() == true)
        m_properties.remainingTimeColorWarning = color.rgb();
    setProperties(m_properties);
}

void StageViewEditor::slotRemainingTimeColorCriticalButtonClicked()
{
    QColor color(m_properties.remainingTimeColorCritical);

    color = QColorDialog::getColor(color, this);
    if (color.isValid() == true)
        m_properties.remainingTimeColorCritical = color.rgb();
    setProperties(m_properties);
}

void StageViewEditor::slotRemainingTimeColorWarningTimeChanged(const QTime& t)
{
    m_properties.warningTime = App::timeToSecs(t);
    if (m_properties.warningTime < m_properties.criticalTime)
        m_remainingTimeColorCriticalSpin->setTime(t);
}

void StageViewEditor::slotRemainingTimeColorCriticalTimeChanged(const QTime& t)
{
    m_properties.criticalTime = App::timeToSecs(t);
    if (m_properties.warningTime < m_properties.criticalTime)
        m_remainingTimeColorWarningSpin->setTime(t);
}

void StageViewEditor::slotRemainingTimeCriticalFlashButtonClicked()
{
    m_properties.criticalFlash =
            m_remainingTimeCriticalFlashButton->isChecked();
}

/*****************************************************************************
 * Next cue page
 *****************************************************************************/

void StageViewEditor::slotNextCueFontButtonClicked()
{
    bool ok = false;
    QFont font;

    font.fromString(m_properties.nextCueFont);
    font = QFontDialog::getFont(&ok, font, this);
    if (ok == true)
        m_properties.nextCueFont = font.toString();
    setProperties(m_properties);
}

void StageViewEditor::slotNextCueColorButtonClicked()
{
    QColor color(m_properties.nextCueColor);

    color = QColorDialog::getColor(color, this);
    if (color.isValid() == true)
        m_properties.nextCueColor = color.rgb();
    setProperties(m_properties);
}
