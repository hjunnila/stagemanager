/*
  StageManager
  stagevieweditor.h

  Copyright (C) Heikki Junnila

  This program is free software; you can redistribute it and/or
  modify it under the terms of the GNU General Public License
  Version 2 as published by the Free Software Foundation.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details. The license is
  in the file "COPYING".

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*/

#ifndef STAGEVIEWEDITOR_H
#define STAGEVIEWEDITOR_H

#include <QResizeEvent>
#include <QDialog>
#include <QTime>

#include "ui_stagevieweditor.h"

#include "stageviewproperties.h"
#include "stageview.h"

class StageViewEditor : public QDialog, public Ui_StageViewEditor
{
    Q_OBJECT

public:
    StageViewEditor(QWidget* parent, const StageViewProperties& properties);
    ~StageViewEditor();

private:
    Q_DISABLE_COPY(StageViewEditor)

protected:
    void initView();

    /*********************************************************************
     * Properties
     *********************************************************************/
public:
    void setProperties(const StageViewProperties& properties);
    const StageViewProperties properties() const;

protected:
    StageViewProperties m_properties;

    /*********************************************************************
     * Background page
     *********************************************************************/
protected slots:
    void slotBGColorButtonClicked();

    /*********************************************************************
     * Cue page
     *********************************************************************/
protected slots:
    void slotCueFontButtonClicked();
    void slotCueColorButtonClicked();

    /*********************************************************************
     * Remaining time page
     *********************************************************************/
protected slots:
    void slotRemainingTimeFontButtonClicked();
    void slotRemainingTimeColorNormalButtonClicked();
    void slotRemainingTimeColorWarningButtonClicked();
    void slotRemainingTimeColorCriticalButtonClicked();

    void slotRemainingTimeColorWarningTimeChanged(const QTime& t);
    void slotRemainingTimeColorCriticalTimeChanged(const QTime& t);

    void slotRemainingTimeCriticalFlashButtonClicked();

    /*********************************************************************
     * Next cue page
     *********************************************************************/
protected slots:
    void slotNextCueFontButtonClicked();
    void slotNextCueColorButtonClicked();
};

#endif
