/*
  StageManager
  stageviewproperties.cpp

  Copyright (C) Heikki Junnila

  This program is free software; you can redistribute it and/or
  modify it under the terms of the GNU General Public License
  Version 2 as published by the Free Software Foundation.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details. The license is
  in the file "COPYING".

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*/

#include <QString>
#include <QColor>
#include <QtXml>

#include "stageviewproperties.h"

/*****************************************************************************
 * Initialization
 *****************************************************************************/

StageViewProperties::StageViewProperties()
{
    bgColor = QColor::fromRgb(0, 0, 0).rgb();

    cueFont = QString("Arial,105,-1,5,50,0,0,0,0,0");
    cueColor = QColor::fromRgb(255, 255, 255).rgb();

    remainingTimeFont = QString("Arial,150,-1,5,50,0,0,0,0,0");
    remainingTimeColorNormal = QColor::fromRgb(0, 255, 0).rgb();
    remainingTimeColorWarning = QColor::fromRgb(255, 0, 0).rgb();
    remainingTimeColorCritical = QColor::fromRgb(255, 0, 0).rgb();

    warningTime = 300;
    criticalTime = 120;
    criticalFlash = true;

    nextCueFont = QString("Arial,72,-1,5,50,0,0,0,0,0");
    nextCueColor = QColor::fromRgb(255, 255, 255).rgb();
}

StageViewProperties::StageViewProperties(const StageViewProperties& properties)
{
    *this = properties;
}

StageViewProperties::~StageViewProperties()
{
}

StageViewProperties& StageViewProperties::operator=(const StageViewProperties& prop)
{
    if (this != &prop)
    {
        bgColor = prop.bgColor;

        cueFont = prop.cueFont;
        cueColor = prop.cueColor;

        remainingTimeFont = prop.remainingTimeFont;
        remainingTimeColorNormal = prop.remainingTimeColorNormal;
        remainingTimeColorWarning = prop.remainingTimeColorWarning;
        remainingTimeColorCritical = prop.remainingTimeColorCritical;

        warningTime = prop.warningTime;
        criticalTime = prop.criticalTime;
        criticalFlash = prop.criticalFlash;

        nextCueFont = prop.nextCueFont;
        nextCueColor = prop.nextCueColor;
    }

    return *this;
}

/*****************************************************************************
 * Load & Save
 *****************************************************************************/

bool StageViewProperties::save(QDomDocument* doc, QDomElement* venue_root)
{
    QDomElement root;
    QDomElement tag;
    QDomElement subtag;
    QDomText text;
    QString str;

    Q_ASSERT(venue_root != NULL);
    Q_ASSERT(doc != NULL);

    /* Properties entry */
    root = doc->createElement(KXMLStageView);
    venue_root->appendChild(root);

    /* Background */
    tag = doc->createElement(KXMLStageViewBackground);
    root.appendChild(tag);

    /* Background color */
    subtag = doc->createElement(KXMLStageViewColor);
    tag.appendChild(subtag);
    text = doc->createTextNode(QString("%1").arg(bgColor));
    subtag.appendChild(text);

    /* Current Cue */
    tag = doc->createElement(KXMLStageViewCue);
    root.appendChild(tag);

    /* Current Cue Font */
    subtag = doc->createElement(KXMLStageViewFont);
    tag.appendChild(subtag);
    text = doc->createTextNode(cueFont);
    subtag.appendChild(text);

    /* Current Cue Color */
    subtag = doc->createElement(KXMLStageViewColor);
    tag.appendChild(subtag);
    text = doc->createTextNode(QString("%1").arg(cueColor));
    subtag.appendChild(text);

    /* Remaining Time */
    tag = doc->createElement(KXMLStageViewRemainingTime);
    root.appendChild(tag);

    /* Remaining Time Font */
    subtag = doc->createElement(KXMLStageViewFont);
    tag.appendChild(subtag);
    text = doc->createTextNode(remainingTimeFont);
    subtag.appendChild(text);

    /* Remaining Time Normal Color */
    subtag = doc->createElement(KXMLStageViewColor);
    tag.appendChild(subtag);
    text = doc->createTextNode(QString("%1").arg(remainingTimeColorNormal));
    subtag.appendChild(text);

    /* Remaining Time Warning Color & Threshold */
    subtag = doc->createElement(KXMLStageViewWarning);
    tag.appendChild(subtag);
    text = doc->createTextNode(QString("%1").arg(remainingTimeColorWarning));
    subtag.appendChild(text);
    subtag.setAttribute(KXMLStageViewThreshold,
                QString("%1").arg(warningTime));

    /* Remaining Time Critical Color & Threshold */
    subtag = doc->createElement(KXMLStageViewCritical);
    tag.appendChild(subtag);
    text = doc->createTextNode(QString("%1").arg(remainingTimeColorCritical));
    subtag.appendChild(text);
    subtag.setAttribute(KXMLStageViewThreshold,
                QString("%1").arg(criticalTime));

    /* Next Cue */
    tag = doc->createElement(KXMLStageViewNextCue);
    root.appendChild(tag);

    /* Next Cue Font */
    subtag = doc->createElement(KXMLStageViewFont);
    tag.appendChild(subtag);
    text = doc->createTextNode(nextCueFont);
    subtag.appendChild(text);

    /* Next Cue Color */
    subtag = doc->createElement(KXMLStageViewColor);
    tag.appendChild(subtag);
    text = doc->createTextNode(QString("%1").arg(nextCueColor));
    subtag.appendChild(text);

    return true;
}

bool StageViewProperties::load(const QDomElement& sv_root)
{
    QDomElement root;
    QDomElement tag;
    QDomNode node;

    if (sv_root.tagName() != KXMLStageView)
    {
        qDebug() << "StageView node not found!";
        return false;
    }

    node = sv_root.firstChild();
    while (node.isNull() == false)
    {
        tag = node.toElement();

        if (tag.tagName() == KXMLStageViewBackground)
        {
            loadFontAndColor(&tag, &bgColor);
        }
        else if (tag.tagName() == KXMLStageViewCue)
        {
            loadFontAndColor(&tag, &cueColor, &cueFont);
        }
        else if (tag.tagName() == KXMLStageViewRemainingTime)
        {
            /* Remaining time contains more stuff than others */
            loadRemainingTime(&tag);
        }
        else if (tag.tagName() == KXMLStageViewNextCue)
        {
            loadFontAndColor(&tag, &nextCueColor, &nextCueFont);
        }
        else
        {
            qDebug() << "Unknown Stage View tag:" << tag.tagName();
        }

        node = node.nextSibling();
    }

    return true;
}

void StageViewProperties::loadFontAndColor(QDomElement* root, QRgb* color,
                       QString* font) const
{
    QDomElement tag;
    QDomNode node;

    node = root->firstChild();
    while (node.isNull() == false)
    {
        tag = node.toElement();
        if (tag.tagName() == KXMLStageViewColor && color != NULL)
            *color = QRgb(tag.text().toUInt());
        else if (tag.tagName() == KXMLStageViewFont && font != NULL)
            *font = tag.text();
        else
            qDebug() << "Unknown Stage View tag:" << tag.tagName();

        node = node.nextSibling();
    }
}

void StageViewProperties::loadRemainingTime(QDomElement* root)
{
    QDomElement tag;
    QDomNode node;

    node = root->firstChild();
    while (node.isNull() == false)
    {
        tag = node.toElement();
        if (tag.tagName() == KXMLStageViewFont)
        {
            remainingTimeFont = tag.text();
        }
        else if (tag.tagName() == KXMLStageViewColor)
        {
            remainingTimeColorNormal = QRgb(tag.text().toUInt());
        }
        else if (tag.tagName() == KXMLStageViewWarning)
        {
            warningTime = tag.attribute(KXMLStageViewThreshold).toInt();
            remainingTimeColorWarning = QRgb(tag.text().toUInt());
        }
        else if (tag.tagName() == KXMLStageViewCritical)
        {
            criticalTime = tag.attribute(KXMLStageViewThreshold).toInt();
            remainingTimeColorCritical = QRgb(tag.text().toUInt());
        }
        else
        {
            qDebug() << "Unknown Stage View tag:" << tag.tagName();
        }

        node = node.nextSibling();
    }
}
