/*
  StageManager
  stageviewproperties.h

  Copyright (C) Heikki Junnila

  This program is free software; you can redistribute it and/or
  modify it under the terms of the GNU General Public License
  Version 2 as published by the Free Software Foundation.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details. The license is
  in the file "COPYING".

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*/

#ifndef STAGEVIEWPROPERTIES_H
#define STAGEVIEWPROPERTIES_H

#include <QString>
#include <QColor>
#include <QFont>
#include <QtXml>

#define KXMLStageView "StageView"
#define KXMLStageViewBackground "Background"
#define KXMLStageViewCue "Cue"
#define KXMLStageViewRemainingTime "RemainingTime"
#define KXMLStageViewThreshold "Threshold"
#define KXMLStageViewNextCue "NextCue"
#define KXMLStageViewFont "Font"
#define KXMLStageViewColor "Color"
#define KXMLStageViewWarning "Warning"
#define KXMLStageViewCritical "Critical"

/**
 * Class for storing the properties of a StageView widget. The properties are
 * QStrings and QRgb's since direct use of QFont and QColor result in very
 * weird font scrambling throughout the whole application.
 */
class StageViewProperties
{
    /*********************************************************************
     * Initialization
     *********************************************************************/
public:
    StageViewProperties();
    StageViewProperties(const StageViewProperties& conf);
    ~StageViewProperties();

    StageViewProperties& operator=(const StageViewProperties& properties);

    /*********************************************************************
     * Properties
     *********************************************************************/
public:
    /** Stage view background */
    QRgb bgColor;

    /** Font used for current cue name */
    QString cueFont;

    /** Color used for current cue name */
    QRgb cueColor;

    /** Font used for current cue remaining time */
    QString remainingTimeFont;

    /** Color used for current cue remaining time, when time is above
        warning & critical threshold */
    QRgb remainingTimeColorNormal;

    /** Color used for current cue remaining time, when time is below
        warning but above critical threshold */
    QRgb remainingTimeColorWarning;

    /** Color used for current cue remaining time, when time is below
        critical threshold */
    QRgb remainingTimeColorCritical;

    /** Warning threshold in seconds */
    int warningTime;

    /** Critical threshold in seconds */
    int criticalTime;

    /** When remaining time is below critical threshold, flash the
        remaining time label colors */
    bool criticalFlash;

    /** Font used for next cue name */
    QString nextCueFont;

    /** Color used for next cue name */
    QRgb nextCueColor;

    /*********************************************************************
     * Load & Save
     *********************************************************************/
public:
    /** Save these properties to the given document, under venue_root tag */
    bool save(QDomDocument* doc, QDomElement* venue_root);

    /** Load contents from the given StageView tag */
    bool load(const QDomElement& sv_root);

protected:
    /** Load the contents of <Font></Font> and <Color></Color> tags */
    void loadFontAndColor(QDomElement* root, QRgb* color,
                  QString* font = NULL) const;

    /** Load the contents of a <RemainingTime></RemainingTime> tag */
    void loadRemainingTime(QDomElement* root);
};

#endif

