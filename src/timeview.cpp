/*
  StageManager
  timeview.cpp

  Copyright (C) Heikki Junnila

  This program is free software; you can redistribute it and/or
  modify it under the terms of the GNU General Public License
  Version 2 as published by the Free Software Foundation.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details. The license is
  in the file "COPYING".

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*/

#include <QColorDialog>
#include <QFontDialog>
#include <QMouseEvent>
#include <QVBoxLayout>
#include <QDateTime>
#include <QSettings>
#include <QPalette>
#include <QMenu>
#include <QLabel>
#include <QColor>
#include <QFont>
#include <QIcon>

#include "timeview.h"
#include "app.h"

TimeView::TimeView(QWidget* parent) : QWidget(parent)
{
    parentWidget()->setWindowTitle(tr("Current time"));
    parentWidget()->setWindowIcon(QIcon(":/gfx/timer.png"));

    new QVBoxLayout(this);

    setAutoFillBackground(true);

    m_timeLabel = new QLabel(this);
    m_timeLabel->setAlignment(Qt::AlignCenter);
    m_timeLabel->setWordWrap(false);
    layout()->addWidget(m_timeLabel);

    loadDefaults();

    slotMasterTick();
}

TimeView::~TimeView()
{
    saveDefaults();
}

void TimeView::loadDefaults()
{
    QSettings settings(KApplicationName, KApplicationName);
    QPalette pal;
    QColor color;
    QString str;
    QFont font;
    QRect r;

    /* Font */
    str = settings.value(KApplicationName + "/timeview/font").toString();
    if (str.length() > 0 && font.fromString(str) == true)
    {
        m_timeLabel->setFont(font);
    }
    else
    {
        /* Set a larger font if nothing was found from settings */
        font = m_timeLabel->font();
        font.setPixelSize(24);
        font.setBold(true);
        m_timeLabel->setFont(font);
    }

    /* Label color */
    str = settings.value(KApplicationName +
                 "/timeview/fontcolor").toString();
    if (str.length() > 0)
    {
        color = QColor(str);
        if (color.isValid() == true)
        {
            pal = m_timeLabel->palette();
            pal.setColor(QPalette::WindowText, color);
            m_timeLabel->setPalette(pal);
        }
    }

    /* Background color */
    str = settings.value(KApplicationName +
                 "/timeview/background").toString();
    if (str.length() > 0)
    {
        color = QColor(str);
        if (color.isValid() == true)
        {
            pal = palette();
            pal.setColor(QPalette::Window, color);
            setPalette(pal);
        }
    }

    /* Window geometry */
    r.setX(settings.value(KApplicationName + "/timeview/x/").toInt());
    r.setY(settings.value(KApplicationName + "/timeview/y/").toInt());
    r.setWidth(settings.value(KApplicationName +
                  "/timeview/width/").toInt());
    r.setHeight(settings.value(KApplicationName +
                   "/timeview/height/").toInt());

    /* Set window geometry if the rect makes sense */
    if (r.isEmpty() == false)
        parentWidget()->setGeometry(r);
    else
        parentWidget()->resize(100, 50);
}

void TimeView::saveDefaults()
{
    QSettings settings(KApplicationName, KApplicationName);
    QRect r = parentWidget()->rect();
    QString str;

    /* Font */
    settings.setValue(KApplicationName + "/timeview/font",
              m_timeLabel->font().toString());

    /* Font color */
    settings.setValue(KApplicationName + "/timeview/fontcolor",
          m_timeLabel->palette().color(QPalette::WindowText).name());

    /* Background color */
    settings.setValue(KApplicationName + "/timeview/background",
              palette().color(QPalette::Window).name());

    /* Window geometry */
    settings.setValue(KApplicationName + "/timeview/x", r.x());
    settings.setValue(KApplicationName + "/timeview/y", r.y());
    settings.setValue(KApplicationName + "/timeview/width", r.width());
    settings.setValue(KApplicationName + "/timeview/height", r.height());
}

/*****************************************************************************
 * UI manipulation
 *****************************************************************************/

void TimeView::slotMasterTick()
{
    m_timeLabel->setText(QTime::currentTime().toString(Qt::ISODate));
}

void TimeView::mousePressEvent(QMouseEvent* e)
{
    if (e->button() == Qt::RightButton)
    {
        QMenu menu(this);
        menu.addAction(QIcon(":/gfx/fonts.png"), tr("Font"),
                   this, SLOT(slotSetFont()));
        menu.addAction(QIcon(":/gfx/color.png"), tr("Font Color"),
                   this, SLOT(slotSetFontColor()));
        menu.addSeparator();
        menu.addAction(QIcon(":/gfx/color.png"), tr("Background Color"),
                   this, SLOT(slotSetBackground()));
        menu.exec(e->globalPos());
    }
}

void TimeView::slotSetFont()
{
    bool ok;
    QFont font;

    font = QFontDialog::getFont(&ok, m_timeLabel->font(), this);
    if (ok == true)
        m_timeLabel->setFont(font);
}

void TimeView::slotSetFontColor()
{
    QColor color;

    color = QColorDialog::getColor(
        m_timeLabel->palette().color(QPalette::WindowText), this);

    if (color.isValid() == true)
    {
        QPalette p = m_timeLabel->palette();
        p.setColor(QPalette::WindowText, color);
        m_timeLabel->setPalette(p);
    }
}

void TimeView::slotSetBackground()
{
    QColor color;

    color = QColorDialog::getColor(palette().color(QPalette::Window), this);
    if (color.isValid() == true)
    {
        QPalette p = palette();
        p.setColor(QPalette::Window, color);
        setPalette(p);
    }
}
