/*
  StageManager
  timeview.h

  Copyright (C) Heikki Junnila

  This program is free software; you can redistribute it and/or
  modify it under the terms of the GNU General Public License
  Version 2 as published by the Free Software Foundation.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details. The license is
  in the file "COPYING".

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*/

#ifndef TIMEVIEW_H
#define TIMEVIEW_H

#include <QWidget>

class QMouseEvent;
class QLabel;

class TimeView : public QWidget
{
    Q_OBJECT

    /*********************************************************************
     * Initialization
     *********************************************************************/
public:
    TimeView(QWidget* parent);
    virtual ~TimeView();

private:
    Q_DISABLE_COPY(TimeView)

protected:
    /** Load default settings for the window */
    void loadDefaults();

    /** Save default settings for the window */
    void saveDefaults();

    /*********************************************************************
     * UI manipulation
     *********************************************************************/
public slots:
    void slotMasterTick();

protected slots:
    void slotSetFont();
    void slotSetFontColor();
    void slotSetBackground();

protected:
    void mousePressEvent(QMouseEvent* e);

protected:
    QLabel* m_timeLabel;
};

#endif
