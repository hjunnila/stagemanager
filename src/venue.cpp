/*
  StageManager
  venue.cpp

  Copyright (C) Heikki Junnila

  This program is free software; you can redistribute it and/or
  modify it under the terms of the GNU General Public License
  Version 2 as published by the Free Software Foundation.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details. The license is
  in the file "COPYING".

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*/

#include <QListIterator>
#include <QMessageBox>
#include <QIODevice>
#include <QDebug>
#include <QtXml>
#include <QFile>

#include "app.h"
#include "cue.h"
#include "venue.h"
#include "stageviewproperties.h"

Venue::Venue(QObject* parent, const QString& name) : QObject(parent)
{
    setState(Stopped);
    setName(name);
    setState(Venue::Stopped);
    setModified(false);
}

Venue::~Venue()
{
}

/*****************************************************************************
 * Name
 *****************************************************************************/

void Venue::setName(const QString& name)
{
    m_name = name;
    setModified(true);
}

QString Venue::name() const
{
    return m_name;
}

/*****************************************************************************
 * Cached cue names
 *****************************************************************************/

void Venue::addCueName(const QString& name)
{
    if (name.isEmpty() == false && m_cueNames.contains(name) == false)
    {
        m_cueNames << name;
        m_cueNames.sort();
    }
}

void Venue::removeCueName(const QString& name)
{
    m_cueNames.removeAll(name);
}

QStringList Venue::cueNames() const
{
    return m_cueNames;
}

/*****************************************************************************
 * Cues
 *****************************************************************************/

void Venue::appendCue(Cue* cue)
{
    Q_ASSERT(cue != NULL);

    m_cues.append(cue);
    connect(cue, SIGNAL(modified(bool)), this, SIGNAL(modified(bool)));
    addCueName(cue->name());
    emit modified(true);
}

void Venue::insertCue(Cue* cue, int position)
{
    Q_ASSERT(cue != NULL);
    Q_ASSERT(position <= m_cues.size());

    m_cues.insert(position, cue);
    connect(cue, SIGNAL(modified(bool)), this, SIGNAL(modified(bool)));
    addCueName(cue->name());
    emit modified(true);
}

void Venue::removeCue(int position)
{
    Q_ASSERT(position < m_cues.size());

    Cue* cue = m_cues.takeAt(position);
    disconnect(cue, SIGNAL(modified(bool)), this, SIGNAL(modified(bool)));
    removeCueName(cue->name());
    delete cue;
    emit modified(true);
}

bool Venue::raiseCueAt(int position)
{
    if (position >= 1 && position < m_cues.size())
    {
        m_cues.move(position, position - 1);
        emit modified(true);
        return true;
    }
    else
    {
        return false;
    }
}

bool Venue::lowerCueAt(int position)
{
    if (position >= 0 && position < (m_cues.size() - 1))
    {
        m_cues.move(position, position + 1);
        emit modified(true);
        return true;
    }
    else
    {
        return false;
    }
}

Cue* Venue::cueAt(int position) const
{
    if (position >= 0 && position < m_cues.size())
        return m_cues.at(position);
    else
        return NULL;
}

QList <Cue*> Venue::cues() const
{
    return m_cues;
}

/*****************************************************************************
 * State
 *****************************************************************************/

Venue::State Venue::state() const
{
    return m_state;
}

void Venue::setState(Venue::State state)
{
    switch (state)
    {
    default:
    case Venue::Stopped:
        m_elapsedTicks = 0;
        m_currentCueNum = 0;
        break;

    case Venue::Running:
        if (m_state == Venue::Stopped)
        {
            m_elapsedTicks = 0;
            resetCueTimes();
        }
        break;

    case Venue::Paused:
        break;
    }

    m_state = state;

    emit stateChanged(state);
}

int Venue::currentCueNumber() const
{
    return m_currentCueNum;
}

Cue* Venue::currentCue() const
{
    if (m_state == Venue::Stopped)
        return NULL;
    else if (currentCueNumber() >= 0 && currentCueNumber() < m_cues.size())
        return cueAt(currentCueNumber());
    else
        return NULL;
}

Cue* Venue::nextCue() const
{
    if (m_state == Venue::Stopped)
        return NULL;
    else
        return cueAt(currentCueNumber() + 1);
}

void Venue::skipToPreviousCue()
{
    if (m_state == Venue::Stopped)
        return;

    /* Get the current cue */
    Cue* cue = cueAt(currentCueNumber());
    if (cue == NULL)
        return;

    /* The current cue has again all of its time left */
    cue->setTimeLeft(cue->duration());

    /* Reduce the amount of time that the current cue has spent
       from overall elapsed time so that the current time points to
       the beginning of the current cue */
    m_elapsedTicks -= (cue->duration() - cue->timeLeft());

    if (currentCueNumber() > 0)
    {
        m_currentCueNum--;

        /* Go to the beginning of the previous cue, if any */
        cue = cueAt(currentCueNumber());
        if (cue == NULL)
            return;

        m_elapsedTicks -= cue->duration();
    }
}

void Venue::skipToNextCue()
{
    if (m_state == Venue::Stopped)
        return;

    /* Get the current cue */
    Cue* cue = cueAt(currentCueNumber());
    if (cue == NULL)
        return;

    /* Add the amount of time still left for the current cue
       to the overall elapsed time */
    m_elapsedTicks += cue->timeLeft();

    /* The current cue no longer has any time left */
    cue->setTimeLeft(0);

    if (currentCueNumber() < (m_cues.size() - 1))
        m_currentCueNum++;
}

void Venue::resetCueTimes()
{
    int venueDuration = 0;

    QListIterator <Cue*> it(m_cues);
    while (it.hasNext())
    {
        Cue* cue = it.next();

        cue->setWaitTime(0);
        cue->setTimeLeft(cue->duration());

        venueDuration += cue->duration();
    }
}

void Venue::slotMasterTick()
{
    int waitTime = 0;
    int cue_num = 0;
    unsigned int venueDuration = 0;

    if (m_state != Venue::Running)
        return;

    waitTime -= m_elapsedTicks;

    QListIterator <Cue*> cue_it(m_cues);
    while (cue_it.hasNext())
    {
        Cue* cue = cue_it.next();

        /* Time until the cue is triggered */
        cue->setWaitTime(waitTime);

        /* Time left until the next cue */
        if (cue->waitTime() <= 0)
            cue->setTimeLeft(cue->duration() + cue->waitTime());

        /* Current cue */
        if (cue->waitTime() <= 0 && cue->timeLeft() > 0)
            m_currentCueNum = cue_num;

        /* Next cue times */
        waitTime += cue->duration();
        venueDuration += cue->duration();

        cue_num++;
    }

    /* If the venue is finished (i.e. no cues left) we can stop
       incrementing the time. */
    if (m_elapsedTicks < venueDuration)
        m_elapsedTicks++;
}

/*****************************************************************************
 * Stage View Properties
 *****************************************************************************/

StageViewProperties Venue::stageViewProperties() const
{
    return m_stageViewProperties;
}

void Venue::setStageViewProperties(const StageViewProperties& properties)
{
    m_stageViewProperties = properties;
}

/*****************************************************************************
 * Load & Save
 *****************************************************************************/

Venue* Venue::loader(QObject* parent, const QString& fileName)
{
    Venue* venue = NULL;

    Q_ASSERT(parent != NULL);
    Q_ASSERT(fileName.isEmpty() == false);

    QFile file(fileName);
    if (file.open(QIODevice::ReadOnly) == true)
    {
        QDomDocument doc;
        QString error;
        int line = 0;
        int col = 0;

        if (doc.setContent(&file, &error, &line, &col) == true)
        {
            venue = new Venue(parent);
            venue->load(doc);
            venue->m_fileName = fileName;
        }
        else
        {
            QString str = tr("%1: %2. Line %3, column %4.");
            QMessageBox::critical(NULL, tr("File load error"),
                str.arg(QDir::toNativeSeparators(fileName))
                   .arg(error).arg(line).arg(col));
        }
    }
    else
    {
        QMessageBox::warning(NULL, tr("File open error"),
                tr("Unable to open %1: %2")
                    .arg(QDir::toNativeSeparators(fileName))
                    .arg(file.errorString()));
    }

    file.close();
    return venue;
}

bool Venue::load(const QDomDocument& doc)
{
    QDomElement root;
    QDomNode node;
    QDomElement tag;

    root = doc.documentElement();

    if (root.tagName() != KXMLVenue)
    {
        qWarning() << "Venue node not found in file!";
        return false;
    }

    node = root.firstChild();
    while (node.isNull() == false)
    {
        tag = node.toElement();

        if (tag.tagName() == KXMLCreator)
        {
            /* Ignore creator information */
        }
        else if (tag.tagName() == KXMLVenueName)
        {
            setName(tag.text());
        }
        else if (tag.tagName() == KXMLCue)
        {
            Cue* cue = new Cue(this);
            if (cue->load(tag) == true)
                appendCue(cue);
            else
                delete cue;
        }
        else if (tag.tagName() == "Event")
        {
            // Legacy support
            QDomNode subnode = node.firstChild();
            while (subnode.isNull() == false)
            {
                QDomElement subtag = subnode.toElement();
                if (subtag.tagName() == KXMLCue)
                {
                    Cue* cue = new Cue(this);
                    if (cue->load(subtag) == true)
                        appendCue(cue);
                    else
                        delete cue;
                }
                else
                {
                    qWarning() << "Unknown event tag" << subtag.tagName();
                }

                subnode = subnode.nextSibling();
            }
        }
        else if (tag.tagName() == KXMLStageView)
        {
            m_stageViewProperties.load(tag);
        }
        else
        {
            qWarning() << "Unknown venue tag:" << tag.tagName();
        }

        node = node.nextSibling();
    }

    return true;
}

bool Venue::save(QString& error, const QString& fileName)
{
    QDomElement root;
    QDomElement tag;
    QDomElement subtag;
    QDomText text;

    /* Change file name if it was given */
    if (fileName.isEmpty() == false)
        m_fileName = fileName;

    if (m_fileName.right(QString(KExtVenue).length()) != QString(KExtVenue))
        m_fileName += QString(KExtVenue);

    /* Attempt to open the given file */
    QFile file(m_fileName);
    if (file.open(QIODevice::WriteOnly) == false)
    {
        error = file.errorString();
        return false;
    }

    /* Create a new DOM document */
    QDomImplementation dom;
    QDomDocument doc(dom.createDocumentType(KXMLVenue, QString(), QString()));

    /* The mother of all XML tags */
    root = doc.createElement(KXMLVenue);
    doc.appendChild(root);

    /* Creator tag */
    tag = doc.createElement(KXMLCreator);
    root.appendChild(tag);

    /* Creator name */
    subtag = doc.createElement(KXMLCreatorName);
    tag.appendChild(subtag);
    text = doc.createTextNode(KApplicationName);
    subtag.appendChild(text);

    /* Creator version */
    subtag = doc.createElement(KXMLCreatorVersion);
    tag.appendChild(subtag);
    text = doc.createTextNode(KApplicationVersion);
    subtag.appendChild(text);

    /* Author */
    subtag = doc.createElement(KXMLCreatorAuthor);
    tag.appendChild(subtag);
    text = doc.createTextNode(getenv("USER"));
    subtag.appendChild(text);

    /* Venue name */
    tag = doc.createElement(KXMLVenueName);
    root.appendChild(tag);
    text = doc.createTextNode(name());
    tag.appendChild(text);

    /* Cues */
    QListIterator <Cue*> cueit(m_cues);
    while (cueit.hasNext() == true)
        cueit.next()->save(&doc, &root);

    /* Stage View Properties */
    m_stageViewProperties.save(&doc, &root);

    /* Write the document into the file */
    file.write(doc.toString().toUtf8());

    /* Close the file */
    file.close();

    /* Reset modified status */
    setModified(false);

    return true;
}

QString Venue::fileName() const
{
    return m_fileName;
}

void Venue::setModified(bool status)
{
    m_modified = status;
    emit modified(status);
}

bool Venue::isModified() const
{
    return m_modified;
}

