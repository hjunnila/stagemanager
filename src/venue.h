/*
  StageManager
  venue.h

  Copyright (C) Heikki Junnila

  This program is free software; you can redistribute it and/or
  modify it under the terms of the GNU General Public License
  Version 2 as published by the Free Software Foundation.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details. The license is
  in the file "COPYING".

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*/

#ifndef VENUE_H
#define VENUE_H

#include <QStringList>
#include <QObject>
#include <QList>
#include <QTime>

#include "stageviewproperties.h"

#define KXMLVenue "Venue"
#define KXMLVenueName "Name"

class QDomDocument;
class Venue;
class Cue;

class Venue : public QObject
{
    Q_OBJECT
    Q_DISABLE_COPY(Venue)

    /*********************************************************************
     * Initialization
     *********************************************************************/
public:
    Venue(QObject* parent = 0, const QString& name = "New Venue");
    virtual ~Venue();

signals:
    /** Emitted whenever this venue's dynamic parameters change */
    void modified(bool status);

    /*********************************************************************
     * Name
     *********************************************************************/
public:
    /** Set the name of the venue */
    void setName(const QString& name);

    /** Get the venue name */
    QString name() const;

private:
    QString m_name;

    /*********************************************************************
     * Cached cue names
     *********************************************************************/
public:
    /** Add a cue name to the cache (unless it's already there) */
    void addCueName(const QString& name);

    /** Remove a cue name from the cache */
    void removeCueName(const QString& name);

    /** Get a list of cached cue names */
    QStringList cueNames() const;

private:
    QStringList m_cueNames;

    /*********************************************************************
     * Cues
     *********************************************************************/
public:
    /** Append a cue to the end of cues */
    void appendCue(Cue* cue);

    /** Insert a cue to the given place */
    void insertCue(Cue* cue, int position);

    /** Remove (and delete) a cue from the given position */
    void removeCue(int position);

    /** Get the cue at the given position. Cue can be modified, but not deleted. */
    Cue* cueAt(int position) const;

    /** Move a cue from the given position before its predecessor */
    bool raiseCueAt(int position);

    /** Move a cue from the given position after its successor */
    bool lowerCueAt(int position);

    /** Get a list of cues */
    QList <Cue*> cues() const;

private:
    QList <Cue*> m_cues;

    /*********************************************************************
     * State
     *********************************************************************/
public:
    enum State { Running, Paused, Stopped };

    /** Get the venue's current state */
    Venue::State state() const;

    /** Set the venue's current state */
    void setState(Venue::State state);

    /** Get the current cue number (-1 if stopped) */
    int currentCueNumber() const;

    /** Get the venue's current cue (NULL if stopped) */
    Cue* currentCue() const;

    /** Get the venue's next cue (NULL if stopped) */
    Cue* nextCue() const;

    /** When the venue is running, skip to the previous cue if possible. */
    void skipToPreviousCue();

    /** When the venue is running, skip to the next cue if possible */
    void skipToNextCue();

    /** Reset waiting time and time left for all cues. */
    void resetCueTimes();

signals:
    void stateChanged(Venue::State s);

protected slots:
    void slotMasterTick();

protected:
    State m_state;
    unsigned long m_elapsedTicks;
    int m_currentCueNum;

    /*********************************************************************
     * Stage View Properties
     *********************************************************************/
public:
    StageViewProperties stageViewProperties() const;
    void setStageViewProperties(const StageViewProperties& properties);

protected:
    StageViewProperties m_stageViewProperties;

    /*********************************************************************
     * Load & Save
     *********************************************************************/
public:
    /** Load and create a new venue from the given file name */
    static Venue* loader(QObject* parent, const QString& fileName);

    /** Save this venue into the given file */
    bool save(QString& error, const QString& fileName = QString::null);

    /** Get this venue's file name */
    QString fileName() const;

    /** Set this venue's modified status (i.e. whether it needs saving) */
    void setModified(bool status);

    /** Check this venue's modified status */
    bool isModified() const;

protected:
    /** Load this venue's contents from the given XML document */
    bool load(const QDomDocument& doc);

protected:
    QString m_fileName;
    bool m_modified;
};

#endif
