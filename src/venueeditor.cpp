/*
  StageManager
  venueeditor.cpp

  Copyright (C) Heikki Junnila

  This program is free software; you can redistribute it and/or
  modify it under the terms of the GNU General Public License
  Version 2 as published by the Free Software Foundation.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details. The license is
  in the file "COPYING".

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*/

#include <QDialogButtonBox>
#include <QTreeWidgetItem>
#include <QInputDialog>
#include <QTreeWidget>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QPushButton>
#include <QSpacerItem>
#include <QMessageBox>
#include <QHeaderView>
#include <QLineEdit>
#include <QSettings>
#include <QToolBar>
#include <QAction>
#include <QLabel>
#include <QList>
#include <QIcon>

#include "venueeditor.h"
#include "cueeditor.h"
#include "cue.h"
#include "venue.h"
#include "app.h"

#define KColumnOrder    0
#define KColumnCue      1
#define KColumnDuration 2

/*****************************************************************************
 * Initialization
 *****************************************************************************/

VenueEditor::VenueEditor(QWidget* parent, Venue* venue) : QWidget(parent)
{
    Q_ASSERT(parent != NULL);
    Q_ASSERT(venue != NULL);

    /* The master layout */
    new QVBoxLayout(this);
    layout()->setSpacing(10);

    setWindowTitle(tr("Venue Editor") + " - " + venue->name());
    parentWidget()->setWindowIcon(QIcon(":/gfx/venue.png"));

    initActions();
    initNameEdit();
    initToolBar();
    initDataView();

    setVenue(venue);

    loadDefaults();
}

VenueEditor::~VenueEditor()
{
    saveDefaults();
}

void VenueEditor::loadDefaults()
{
    QSettings settings(KApplicationName, KApplicationName);
    QSize s;

    s.setWidth(settings.value(KApplicationName + "/venueeditor/width/").toInt());
    s.setHeight(settings.value(KApplicationName + "/venueeditor/height/").toInt());

    if (s.isValid() == true)
        resize(s);
    else
        resize(400, 300);
}

void VenueEditor::saveDefaults()
{
    QSettings settings(KApplicationName, KApplicationName);
    QSize s = size();

    settings.setValue(KApplicationName + "/venueeditor/width", s.width());
    settings.setValue(KApplicationName + "/venueeditor/height", s.height());
}

/*****************************************************************************
 * Actions
 *****************************************************************************/

void VenueEditor::initActions()
{
    m_addAction = new QAction(QIcon(":/gfx/add.png"), tr("Add cue"), this);
    connect(m_addAction, SIGNAL(triggered(bool)), this, SLOT(slotAdd()));

    m_removeAction = new QAction(QIcon(":/gfx/remove.png"), tr("Remove cue"), this);
    connect(m_removeAction, SIGNAL(triggered(bool)), this, SLOT(slotRemove()));

    m_editAction = new QAction(QIcon(":/gfx/edit.png"), tr("Edit cue"), this);
    connect(m_editAction, SIGNAL(triggered(bool)), this, SLOT(slotEdit()));

    m_raiseAction = new QAction(QIcon(":/gfx/raise.png"), tr("Raise cue"), this);
    connect(m_raiseAction, SIGNAL(triggered(bool)), this, SLOT(slotRaise()));

    m_lowerAction = new QAction(QIcon(":/gfx/lower.png"), tr("Lower cue"), this);
    connect(m_lowerAction, SIGNAL(triggered(bool)), this, SLOT(slotLower()));
}

/*****************************************************************************
 * UI initialization
 *****************************************************************************/

void VenueEditor::initNameEdit()
{
    /* Create a horizontal layout for name label and name edit and put
       them first to the dialog */
    QHBoxLayout* hbox = new QHBoxLayout();
    qobject_cast<QVBoxLayout*> (layout())->addLayout(hbox);

    m_nameLabel = new QLabel(this);
    hbox->addWidget(m_nameLabel);
    m_nameLabel->setText(tr("Name"));

    m_nameEdit = new QLineEdit(this);
    hbox->addWidget(m_nameEdit);
    connect(m_nameEdit, SIGNAL(textEdited(QString)), this, SLOT(slotNameEdited(QString)));
}

void VenueEditor::initToolBar()
{
    /* Create a toolbar, put it below the name edit and attach some
       actions to the toolbar */
    m_toolBar = new QToolBar(this);
    layout()->addWidget(m_toolBar);

    m_toolBar->addAction(m_addAction);
    m_toolBar->addAction(m_removeAction);
    m_toolBar->addAction(m_editAction);
    m_toolBar->addSeparator();
    m_toolBar->addAction(m_raiseAction);
    m_toolBar->addAction(m_lowerAction);
}

void VenueEditor::initDataView()
{
    /* Create a tree widget for cues that take part in this venue */
    m_cueTree = new QTreeWidget(this);
    layout()->addWidget(m_cueTree);
    m_cueTree->setRootIsDecorated(false);
    m_cueTree->setAllColumnsShowFocus(true);
    m_cueTree->header()->setSectionsClickable(false);
    m_cueTree->header()->setSectionResizeMode(QHeaderView::Stretch);

    QStringList columns;
    columns << tr("Order") << tr("Cue") << tr("Duration");
    m_cueTree->setHeaderLabels(columns);

    connect(m_cueTree, SIGNAL(itemDoubleClicked(QTreeWidgetItem*,int)), this, SLOT(slotEdit()));
}

/*****************************************************************************
 * UI manipulation
 *****************************************************************************/

void VenueEditor::slotAdd()
{
    Cue* cue = new Cue(m_venue);
    cue->setDuration(1800); // TODO: Put some default setting here

    CueEditor editor(this, cue, m_venue);
    if (editor.exec() == QDialog::Accepted)
    {
        QTreeWidgetItem* item = m_cueTree->currentItem();

        if (item == NULL)
        {
            m_venue->appendCue(cue);
            updateCueItem(cue, new QTreeWidgetItem(m_cueTree));
        }
        else
        {
            /* Insert after the selected item */
            int pos = m_cueTree->indexOfTopLevelItem(item) + 1;
            m_venue->insertCue(cue, pos);

            item = new QTreeWidgetItem();
            m_cueTree->insertTopLevelItem(pos, item);
            updateCueItem(cue, item);
        }

        updateOrderNumbers();
    }
    else
    {
        delete cue;
    }
}

void VenueEditor::slotRemove()
{
    QTreeWidgetItem* item = m_cueTree->currentItem();
    int order, r;
    QString str;

    if (item == NULL)
        return;

    order = m_cueTree->indexOfTopLevelItem(item);
    str.setNum(order + 1);

    r = QMessageBox::question(this,
                  tr("Remove cue"),
                  tr("Remove cue %1 from venue %2 position %3?")
                  .arg(item->text(KColumnCue))
                  .arg(m_venue->name())
                  .arg(str),
                  QMessageBox::Yes, QMessageBox::No);
    if (r == QMessageBox::Yes)
    {
        m_venue->removeCue(order);
        delete item;
        updateOrderNumbers();
    }
}

void VenueEditor::slotEdit()
{
    QTreeWidgetItem* item = m_cueTree->currentItem();
    if (item == NULL)
        return;

    /* Get the cue from the selected position */
    Cue* cue = m_venue->cueAt(m_cueTree->indexOfTopLevelItem(item));
    Q_ASSERT(cue != NULL);

    CueEditor editor(this, cue, m_venue);
    if (editor.exec() == QDialog::Accepted)
        updateCueItem(cue, item);
}

void VenueEditor::slotRaise()
{
    QTreeWidgetItem* item = m_cueTree->currentItem();
    if (item == NULL)
        return;

    int order = m_cueTree->indexOfTopLevelItem(item);
    if (order <= 0)
        return;

    m_venue->raiseCueAt(order);

    item = m_cueTree->takeTopLevelItem(order);
    m_cueTree->insertTopLevelItem(order - 1, item);
    m_cueTree->setCurrentItem(item);

    updateOrderNumbers();
}

void VenueEditor::slotLower()
{
    QTreeWidgetItem* item = m_cueTree->currentItem();
    if (item == NULL)
        return;

    int order = m_cueTree->indexOfTopLevelItem(item);
    if (order >= (m_cueTree->topLevelItemCount() - 1))
        return;

    m_venue->lowerCueAt(order);

    item = m_cueTree->takeTopLevelItem(order);
    m_cueTree->insertTopLevelItem(order + 1, item);
    m_cueTree->setCurrentItem(item);

    updateOrderNumbers();
}

void VenueEditor::slotNameEdited(const QString& text)
{
    m_venue->setName(text);
}

/*****************************************************************************
 * Venue
 *****************************************************************************/

void VenueEditor::setVenue(Venue* venue)
{
    Q_ASSERT(venue != NULL);
    m_venue = venue;

    m_nameEdit->setText(venue->name());

    m_cueTree->clear();
    QListIterator <Cue*> it(venue->cues());
    while (it.hasNext() == true)
        updateCueItem(it.next(), new QTreeWidgetItem(m_cueTree));
}

void VenueEditor::updateCueItem(Cue* cue, QTreeWidgetItem* item)
{
    Q_ASSERT(cue != NULL);
    Q_ASSERT(item != NULL);

    /* Order */
    QString str = QString::number(m_cueTree->indexOfTopLevelItem(item) + 1);
    item->setText(KColumnOrder, str);

    /* Cue name */
    item->setText(KColumnCue, cue->name());

    /* Duration */
    QTime time = App::secsToTime(cue->duration());
    item->setText(KColumnDuration, time.toString(Qt::ISODate));
}

void VenueEditor::updateOrderNumbers()
{
    QTreeWidgetItemIterator it(m_cueTree);
    while (*it != NULL)
    {
        QString str = QString::number(m_cueTree->indexOfTopLevelItem(*it) + 1);
        (*it)->setText(KColumnOrder, str);
        ++it;
    }
}
