/*
  StageManager
  venueeditor.h

  Copyright (C) Heikki Junnila

  This program is free software; you can redistribute it and/or
  modify it under the terms of the GNU General Public License
  Version 2 as published by the Free Software Foundation.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details. The license is
  in the file "COPYING".

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*/

#ifndef VENUEEDITOR_H
#define VENUEEDITOR_H

#include <QWidget>

class QLineEdit;
class QToolBar;
class QAction;
class QLabel;
class QTreeWidget;
class QPushButton;
class QTreeWidgetItem;
class QDialogButtonBox;

class Cue;
class Venue;

class VenueEditor : public QWidget
{
    Q_OBJECT
    Q_DISABLE_COPY(VenueEditor)

    /*********************************************************************
     * Initialization
     *********************************************************************/
public:
    VenueEditor(QWidget* parent, Venue* venue);
    virtual ~VenueEditor();

protected:
    /** Load default settings for the window */
    void loadDefaults();

    /** Save default settings for the window */
    void saveDefaults();

    /*********************************************************************
     * Actions
     *********************************************************************/
protected:
    void initActions();

protected:
    QAction* m_addAction;
    QAction* m_removeAction;
    QAction* m_editAction;
    QAction* m_raiseAction;
    QAction* m_lowerAction;

    /*********************************************************************
     * UI manipulation
     *********************************************************************/
protected:
    void initNameEdit();
    void initToolBar();
    void initDataView();

protected slots:
    void slotAdd();
    void slotRemove();
    void slotEdit();
    void slotRaise();
    void slotLower();
    void slotNameEdited(const QString& text);

protected:
    QLabel* m_nameLabel;
    QLineEdit* m_nameEdit;
    QToolBar* m_toolBar;
    QTreeWidget* m_cueTree;
    QDialogButtonBox* m_buttonBox;

    /*********************************************************************
     * Venue
     *********************************************************************/
public:
    void setVenue(Venue* venue);

protected:
    void updateCueItem(Cue* cue, QTreeWidgetItem* item);
    void updateOrderNumbers();

protected:
    Venue* m_venue;
};

#endif
