/*
  StageManager
  venuemonitor.cpp

  Copyright (C) Heikki Junnila

  This program is free software; you can redistribute it and/or
  modify it under the terms of the GNU General Public License
  Version 2 as published by the Free Software Foundation.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details. The license is
  in the file "COPYING".

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*/

#include <QTreeWidgetItem>
#include <QTreeWidget>
#include <QHeaderView>
#include <QMessageBox>
#include <QVBoxLayout>
#include <QDockWidget>
#include <QToolBar>
#include <QPalette>
#include <QAction>
#include <QBrush>
#include <QColor>
#include <QDebug>

#include "venuemonitor.h"
#include "venue.h"
#include "cue.h"
#include "app.h"

#define KColumnName     0
#define KColumnTimeLeft 1
#define KColumnWaitTime 2
#define KColumnPointer  3

VenueMonitor::VenueMonitor(QWidget* parent, Venue* venue) : QWidget(parent)
{
    m_venue = NULL;
    m_currentItem = NULL;

    initView();
    setVenue(venue);
}

VenueMonitor::~VenueMonitor()
{
}

/*****************************************************************************
 * Venue
 *****************************************************************************/

void VenueMonitor::setVenue(Venue* venue)
{
    m_venue = venue;
    if (m_venue != NULL)
    {
        connect(m_venue, SIGNAL(modified(bool)),
            this, SLOT(slotVenueModified(bool)));
    }

    createItems();
}

void VenueMonitor::slotVenueModified(bool status)
{
    if (status == true)
        createItems();
}

/*****************************************************************************
 * UI Manipulation
 *****************************************************************************/

void VenueMonitor::initView()
{
    QStringList labels;

    /* Set the text on show/hide button */
    qobject_cast<QDockWidget*>
        (parentWidget())->toggleViewAction()->setText(tr("Close"));

    /* Connect to the show/hide button's triggered signal so that this
       can be deleted */
    connect(static_cast<QDockWidget*> (parentWidget())->toggleViewAction(),
        SIGNAL(triggered(bool)), this, SLOT(slotClose()));

    new QVBoxLayout(this);

    m_tree = new QTreeWidget(this);
    layout()->addWidget(m_tree);

    labels << tr("Cue") << tr("Time left") << tr("Waiting time");
    m_tree->setHeaderLabels(labels);
    m_tree->setSelectionMode(QAbstractItemView::NoSelection);
}

void VenueMonitor::createItems()
{
    m_tree->clear();
    if (m_venue == NULL)
        return;

    QListIterator <Cue*> it(m_venue->cues());
    while (it.hasNext() == true)
        updateCueItem(it.next(), new QTreeWidgetItem(m_tree));
}

void VenueMonitor::updateCueItem(Cue* cue, QTreeWidgetItem* item)
{
    Q_ASSERT(cue != NULL);
    Q_ASSERT(item != NULL);

    item->setText(KColumnName, cue->name());

    /* Time left */
    int seconds = cue->timeLeft();
    if (seconds <= 0)
        seconds = 0;
    QString str = App::secsToTime(seconds).toString(Qt::ISODate);
    item->setText(KColumnTimeLeft, str);

    /* Wait time */
    seconds = cue->waitTime();
    if (seconds <= 0)
        seconds = 0;
    str = App::secsToTime(seconds).toString(Qt::ISODate);
    item->setText(KColumnWaitTime, str);

    /* Cue pointer */
    item->setData(KColumnPointer, Qt::UserRole, (qulonglong) cue);

    /* Current item, set only if running/paused */
    if ((m_venue->state() == Venue::Running ||
         m_venue->state() == Venue::Paused) &&
        (cue->timeLeft() > 0 && cue->waitTime() <= 0))
    {
        /* Scroll to the current item only when it changes */
        if (item != m_currentItem)
            m_tree->scrollToItem(item);
        m_currentItem = item;

        item->setIcon(KColumnName, QIcon(":/gfx/current.png"));
        item->setBackground(KColumnName,
                    QBrush(palette().highlight()));
        item->setBackground(KColumnTimeLeft,
                    QBrush(palette().highlight()));
        item->setBackground(KColumnWaitTime,
                    QBrush(palette().highlight()));

        item->setForeground(KColumnName,
                    QBrush(palette().highlightedText()));
        item->setForeground(KColumnTimeLeft,
                    QBrush(palette().highlightedText()));
        item->setForeground(KColumnWaitTime,
                    QBrush(palette().highlightedText()));
    }
    else
    {
        item->setIcon(KColumnName, QIcon());

        item->setBackground(KColumnName, QBrush());
        item->setBackground(KColumnTimeLeft, QBrush());
        item->setBackground(KColumnWaitTime, QBrush());

        item->setForeground(KColumnName, QBrush());
        item->setForeground(KColumnTimeLeft, QBrush());
        item->setForeground(KColumnWaitTime, QBrush());
    }
}

void VenueMonitor::slotClose()
{
    parentWidget()->deleteLater();
}

void VenueMonitor::slotMasterTick()
{
    if (m_venue == NULL)
        return;

    QTreeWidgetItemIterator it(m_tree);
    while (*it != NULL)
    {
        Cue* cue = reinterpret_cast<Cue*>
            ((*it)->data(KColumnPointer, Qt::UserRole).toULongLong());
        if (cue != NULL)
            updateCueItem(cue, *it);
        it++;
    }
}
