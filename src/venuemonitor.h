/*
  StageManager
  venuemonitor.h

  Copyright (C) Heikki Junnila

  This program is free software; you can redistribute it and/or
  modify it under the terms of the GNU General Public License
  Version 2 as published by the Free Software Foundation.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details. The license is
  in the file "COPYING".

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*/

#ifndef VENUEMONITOR_H
#define VENUEMONITOR_H

#include <QPointer>
#include <QWidget>
#include "venue.h"

class QTreeWidgetItem;
class QTreeWidget;
class QCloseEvent;
class QToolBar;
class QAction;

class Artist;
class Cue;

class VenueMonitor : public QWidget
{
    Q_OBJECT

public:
    VenueMonitor(QWidget* parent, Venue* venue);
    ~VenueMonitor();

private:
    Q_DISABLE_COPY(VenueMonitor)

    /*********************************************************************
     * Venue
     *********************************************************************/
public:
    void setVenue(Venue* venue);

protected slots:
    void slotVenueModified(bool status);

    /*********************************************************************
     * UI Manipulation
     *********************************************************************/
protected:
    void initView();
    void createItems();

    void updateCueItem(Cue* cue, QTreeWidgetItem* item);

protected slots:
    void slotClose();
    void slotMasterTick();

protected:
    QToolBar* m_toolBar;
    QTreeWidget* m_tree;
    QTreeWidgetItem* m_currentItem;
    QPointer<Venue> m_venue;
};

#endif
