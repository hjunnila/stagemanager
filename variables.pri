CONFIG += release warn_on
CONFIG -= debug

unix:!macx {
    BINDIR = /usr/bin
    DATADIR = /usr/share/stagemanager
    DOCSDIR = $$DATADIR/documentation
}

macx {
    QTPLUGIN += qjpeg qgif
    INSTALLROOT = ~/StageManager.app/Contents
    BINDIR = $$INSTALLROOT/MacOS
    DATADIR = $$INSTALLROOT/Resources
    DOCSDIR = $$DATADIR/Documentation
    LIBSDIR = Frameworks
}

win32 {
    BINDIR = $$(SystemDrive)/StageManager
    DATADIR = $$BINDIR
    DOCSDIR = $$DATADIR/documentation
}
